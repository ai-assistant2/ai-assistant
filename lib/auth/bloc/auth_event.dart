part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class SignIn extends AuthEvent {
  final User user;
  const SignIn({required this.user});
}

class SignInWithEmail extends AuthEvent {
  final String email;
  final String password;

  const SignInWithEmail(this.email, this.password);
}

class AppLoaded extends AuthEvent {}

class SignOut extends AuthEvent {}

class AccountDeleted extends AuthEvent {}

class SignSelectionChanged extends AuthEvent {
  final bool showSignIn;
  const SignSelectionChanged({required this.showSignIn});
}
