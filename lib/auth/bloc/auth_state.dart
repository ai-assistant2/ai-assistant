part of 'auth_bloc.dart';

enum AuthStatus { initial, loading, authenticated, unauthenticated, authError }

class AuthState extends Equatable {
  const AuthState(
      {this.showSignIn = false,
      this.errorMessage = '',
      this.status = AuthStatus.initial});

  final AuthStatus status;
  final String errorMessage;
  final bool showSignIn;

  AuthState copyWith(
      {AuthStatus? status, String? errorMessage, bool? showSignIn}) {
    return AuthState(
        status: status ?? this.status,
        showSignIn: showSignIn ?? this.showSignIn,
        errorMessage: errorMessage ?? this.errorMessage);
  }

  @override
  List<Object> get props => [showSignIn, status, errorMessage];
}
