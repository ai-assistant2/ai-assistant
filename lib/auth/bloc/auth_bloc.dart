import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../repository/auth_repository.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepository authRepository;
  //late SharedPreferences _prefs;

  AuthBloc({
    required this.authRepository,
  }) : super(const AuthState()) {
    on<AppLoaded>(_onAppLoaded);
    on<SignOut>(_onSignOut);
    on<SignIn>(_onSignIn);
    on<SignSelectionChanged>(_onSignSelectionChanged);
    on<AccountDeleted>(_onAccountDeleted);
  }

  void _onAppLoaded(AppLoaded event, Emitter emit) async {
    emit(state.copyWith(status: AuthStatus.loading));

    SharedPreferences prefs = await SharedPreferences.getInstance();

    bool openCountExist = prefs.containsKey('openCount');
    if (openCountExist) {
      prefs.setInt('openCount', 1);
    } else {
      int? openCount = prefs.getInt('openCount');
      openCount = openCount ?? 0 + 1;
      prefs.setInt('openCount', openCount);
    }

    final String? user = prefs.getString('userID');

    if (user != null) {
      await prefs.setString('userID', user);

      emit(state.copyWith(
        status: AuthStatus.authenticated,
      ));
    } else {
      emit(state.copyWith(
        status: AuthStatus.unauthenticated,
      ));
    }
  }

  void _onSignIn(SignIn event, Emitter emit) async {
    emit(state.copyWith(status: AuthStatus.loading));

    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setString('userID', event.user.uid);

    emit(state.copyWith(status: AuthStatus.authenticated));
  }

  void _onSignOut(SignOut event, Emitter emit) async {
    emit(state.copyWith(status: AuthStatus.loading));
    await authRepository.signOut();
    emit(state.copyWith(status: AuthStatus.unauthenticated));
  }

  void _onSignSelectionChanged(SignSelectionChanged event, Emitter emit) {
    emit(state.copyWith(
        status: AuthStatus.unauthenticated, showSignIn: event.showSignIn));
  }

  void _onAccountDeleted(AccountDeleted event, Emitter emit) async {
    emit(state.copyWith(status: AuthStatus.loading));
    await authRepository.deleteAccount();
    emit(state.copyWith(status: AuthStatus.unauthenticated));
  }
}
