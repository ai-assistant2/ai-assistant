part of 'purchases_bloc.dart';

abstract class PurchasesEvent extends Equatable {
  const PurchasesEvent();

  @override
  List<Object> get props => [];
}

class PurchasesInitialized extends PurchasesEvent {}

class SubscriptionStatusFetched extends PurchasesEvent {}

class OfferingsFetched extends PurchasesEvent {}

class SubscriptionUpdated extends PurchasesEvent {
  final int index;
  const SubscriptionUpdated({required this.index});
}

class SubscriptionRestored extends PurchasesEvent {}

class SubscriptionPurchased extends PurchasesEvent {
  final Package package;
  const SubscriptionPurchased({required this.package});
}
