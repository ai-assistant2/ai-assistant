part of 'purchases_bloc.dart';

enum PurchasesStatus { initial, purchased, fail, cancel, loading, restored }

class PurchasesState extends Equatable {
  const PurchasesState(
      {this.offerings,
      this.isRestored = false,
      this.status = PurchasesStatus.initial,
      this.message = '',
      this.isPro = false,
      this.selectedIndex = 0});

  final Offerings? offerings;
  final PurchasesStatus status;
  final bool isRestored;
  final String message;
  final int? selectedIndex;
  final bool isPro;

  PurchasesState copyWith(
      {Offerings? offerings,
      bool? isRestored,
      int? selectedIndex,
      PurchasesStatus? status,
      String? message,
      bool? isPro}) {
    return PurchasesState(
        offerings: offerings ?? this.offerings,
        status: status ?? this.status,
        selectedIndex: selectedIndex ?? this.selectedIndex,
        message: message ?? this.message,
        isRestored: isRestored ?? this.isRestored,
        isPro: isPro ?? this.isPro);
  }

  @override
  List<Object?> get props =>
      [offerings, isRestored, status, message, selectedIndex, isPro];
}
