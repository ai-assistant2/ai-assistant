import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:purchases_flutter/purchases_flutter.dart';

import '../../repository/purchases_repository.dart';

part 'purchases_event.dart';
part 'purchases_state.dart';

class PurchasesBloc extends Bloc<PurchasesEvent, PurchasesState> {
  PurchasesRepository purchasesRepository;
  Offerings? _offerings;

  PurchasesBloc({required this.purchasesRepository})
      : super(const PurchasesState()) {
    on<OfferingsFetched>(_onOfferingsFetched);
    on<SubscriptionPurchased>(_onSubscriptionPurchased);
    on<SubscriptionRestored>(_onSubscriptionRestored);
    on<SubscriptionUpdated>(_onSubscriptionUpdated);
    on<SubscriptionStatusFetched>(_onSubscriptionStatusFetched);
    on<PurchasesInitialized>(_onPurchasesInitialized);
  }

  Future _onPurchasesInitialized(
      PurchasesInitialized event, Emitter<PurchasesState> emit) async {
    await purchasesRepository.initPlatformState();
    bool isPro = await purchasesRepository.getSubscriptionStatus();
    _offerings = await purchasesRepository.getOfferings();
    debugPrint('the user is $isPro');

    emit(state.copyWith(isPro: isPro, offerings: _offerings));
  }

  Future _onSubscriptionStatusFetched(
      SubscriptionStatusFetched event, Emitter<PurchasesState> emit) async {
    await purchasesRepository.checkIfInitialized();
    bool isPro = await purchasesRepository.getSubscriptionStatus();

    emit(state.copyWith(isPro: isPro));
  }

  Future _onSubscriptionUpdated(
      SubscriptionUpdated event, Emitter<PurchasesState> emit) async {
    emit(state.copyWith(selectedIndex: event.index));
  }

  Future _onOfferingsFetched(
      OfferingsFetched event, Emitter<PurchasesState> emit) async {
    _offerings == null
        ? _offerings = await purchasesRepository.getOfferings()
        : null;

    emit(state.copyWith(offerings: _offerings));
  }

  Future _onSubscriptionRestored(
      SubscriptionRestored event, Emitter<PurchasesState> emit) async {
    emit(state.copyWith(status: PurchasesStatus.loading));

    bool isRestored = await purchasesRepository.restoreSubscription();
    emit(state.copyWith(
        isPro: isRestored ? true : false,
        status: isRestored ? PurchasesStatus.restored : PurchasesStatus.fail,
        message: isRestored ? 'restored' : 'not-restored'));
  }

  Future _onSubscriptionPurchased(
      SubscriptionPurchased event, Emitter<PurchasesState> emit) async {
    emit(state.copyWith(status: PurchasesStatus.loading));
    try {
      bool isPurchased = await purchasesRepository.makePurchase(event.package);

      emit(state.copyWith(
          isPro: isPurchased ? true : false,
          status:
              isPurchased ? PurchasesStatus.purchased : PurchasesStatus.cancel,
          message: isPurchased ? 'purchased' : 'purchased-cancelled'));
    } on PlatformException catch (e) {
      emit(state.copyWith(status: PurchasesStatus.fail));
      var errorCode = PurchasesErrorHelper.getErrorCode(e);

      if (errorCode != PurchasesErrorCode.purchaseCancelledError) {}
    }
  }
}
