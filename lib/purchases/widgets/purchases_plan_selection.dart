import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../bloc/purchases_bloc.dart';

class PurchasesPlanSelection extends StatelessWidget {
  const PurchasesPlanSelection({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PurchasesBloc, PurchasesState>(
      builder: (context, state) {
        return SliverPadding(
          padding: const EdgeInsets.only(top: 10),
          sliver: SliverToBoxAdapter(
            child: SizedBox(
                child: Center(
              child: state.offerings != null
                  ? Column(
                      children: List.generate(
                          state.offerings?.current?.availablePackages.length ??
                              0,
                          (index) => Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: InkWell(
                                  onTap: () => context
                                      .read<PurchasesBloc>()
                                      .add(SubscriptionUpdated(index: index)),
                                  child: PurchasesPlanItem(
                                    index: index,
                                    isSelected: state.selectedIndex == index
                                        ? true
                                        : false,
                                    period:
                                        _PurchasesPrice.getPricePeriod(context)
                                            .elementAt(index),
                                    price: state
                                            .offerings
                                            ?.current
                                            ?.availablePackages[index]
                                            .storeProduct
                                            .priceString ??
                                        '0',
                                  )))),
                    )
                  : const CircularProgressIndicator(),
            )),
          ),
        );
      },
    );
  }
}

class PurchasesPlanItem extends StatelessWidget {
  const PurchasesPlanItem(
      {super.key,
      required this.isSelected,
      required this.price,
      required this.period,
      required this.index});

  final bool isSelected;
  final String price;
  final String period;
  final int index;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.08,
      child: Card(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
              side: isSelected
                  ? const BorderSide(color: Color(0xFFFFDF00), width: 4.0)
                  : BorderSide.none),
          child: Padding(
            padding: const EdgeInsets.only(left: 20, top: 10, bottom: 10),
            child: Row(
              children: [
                Text('$period:  ',
                    style: Theme.of(context).textTheme.headlineSmall),
                Text(price,
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge
                        ?.copyWith(fontWeight: FontWeight.bold)),
                index == 2
                    ? Row(
                        children: [
                          const SizedBox(width: 2),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.35,
                            child: Card(
                              shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),
                              color: Theme.of(context).colorScheme.secondary,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Flexible(
                                  child: Text(
                                    AppLocalizations.of(context)!.best_value,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 5,
                          ),
                          Text('🚀',
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineMedium
                                  ?.copyWith(
                                    fontWeight: FontWeight.bold,
                                  ))
                        ],
                      )
                    : const SizedBox.shrink()
              ],
            ),
          )),
    );
  }
}

class _PurchasesPrice {
  static List<String> getPricePeriod(BuildContext context) {
    return [
      AppLocalizations.of(context)!.weekly,
      AppLocalizations.of(context)!.monthly,
      AppLocalizations.of(context)!.yearly
    ];
  }
}
