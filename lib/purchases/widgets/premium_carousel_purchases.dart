import 'package:flutter/material.dart';
import 'package:flutter_carousel_slider/carousel_slider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class PremiumCarouselPurchases extends StatelessWidget {
  const PremiumCarouselPurchases({super.key});

  @override
  Widget build(BuildContext context) {
    List<Map<String, dynamic>> getCarousel = _CarouselData.getCarousel(context);
    return SliverPadding(
      padding: const EdgeInsets.only(top: 10),
      sliver: SliverToBoxAdapter(
        child: SizedBox(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.1,
          child: CarouselSlider.builder(
              unlimitedMode: true,
              autoSliderTransitionCurve: Curves.easeIn,
              enableAutoSlider: true,
              autoSliderDelay: const Duration(seconds: 4),
              autoSliderTransitionTime: const Duration(seconds: 1),
              viewportFraction: 0.6,
              slideBuilder: (index) => _CarouselItem(
                    map: getCarousel[index],
                  ),
              itemCount: getCarousel.length),
        ),
      ),
    );
  }
}

class _CarouselItem extends StatelessWidget {
  final Map<String, dynamic> map;
  const _CarouselItem({required this.map});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              map.entries.elementAt(1).value,
              height: 50,
            ),
            const SizedBox(
              width: 5,
            ),
            Flexible(
              child: Text(
                map.entries.last.value,
                style: Theme.of(context)
                    .textTheme
                    .titleMedium
                    ?.copyWith(fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _CarouselData {
  static List<Map<String, dynamic>> getCarousel(BuildContext context) {
    return [
      {
        "id": 0,
        "url": 'assets/icon/imessage.png',
        "content": AppLocalizations.of(context)?.imessage_integration,
      },
      {
        "id": 1,
        "url": 'assets/icon/chat.png',
        "content": AppLocalizations.of(context)?.saved_chats,
      },
      {
        "id": 2,
        "url": 'assets/icon/data.png',
        "content": AppLocalizations.of(context)?.ai_with_web_access,
      },
      {
        "id": 3,
        "url": 'assets/icon/devices.png',
        "content": AppLocalizations.of(context)?.available_many_devices,
      },
      {
        "id": 4,
        "url": 'assets/icon/unknown.png',
        "content": AppLocalizations.of(context)?.anonymous
      },
      {
        "id": 5,
        "url": 'assets/icon/lock.png',
        "content": AppLocalizations.of(context)?.remove_all_ads
      },
    ];
  }
}
