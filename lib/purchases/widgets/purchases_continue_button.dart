import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../bloc/purchases_bloc.dart';

class ContinueButton extends StatelessWidget {
  const ContinueButton({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PurchasesBloc, PurchasesState>(
      builder: (context, state) {
        return SliverPadding(
          padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
          sliver: SliverToBoxAdapter(
              child: CupertinoButton(
                  borderRadius: const BorderRadius.all(Radius.circular(20)),
                  color: Theme.of(context).primaryColor,
                  child: state.offerings?.current
                              ?.availablePackages[state.selectedIndex ?? 0] !=
                          null
                      ? Text(
                          AppLocalizations.of(context)!.continue_button,
                          style: Theme.of(context)
                              .textTheme
                              .titleLarge
                              ?.copyWith(color: CupertinoColors.white),
                        )
                      : const Center(
                          child: CircularProgressIndicator(),
                        ),
                  onPressed: () {
                    state.offerings != null
                        ? context.read<PurchasesBloc>().add(
                            SubscriptionPurchased(
                                package:
                                    state.offerings!.current!.availablePackages[
                                        state.selectedIndex ?? 0]))
                        : null;
                  })),
        );
      },
    );
  }
}
