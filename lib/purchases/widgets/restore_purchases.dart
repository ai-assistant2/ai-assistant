import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../bloc/purchases_bloc.dart';

class RestorePurchases extends StatelessWidget {
  const RestorePurchases({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PurchasesBloc, PurchasesState>(
      builder: (context, state) {
        return SliverPadding(
          padding: const EdgeInsets.only(bottom: 40),
          sliver: SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Center(
                child: InkWell(
                    onTap: () {
                      context.read<PurchasesBloc>().add(SubscriptionRestored());
                    },
                    child: Text(
                      AppLocalizations.of(context)!.restore_purchases,
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium
                          ?.copyWith(color: CupertinoColors.activeBlue),
                    )),
              ),
            ),
          ),
        );
      },
    );
  }
}
