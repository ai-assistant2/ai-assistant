import 'package:ai_assistant/purchases/widgets/cancel_anytime_purchases.dart';
import 'package:ai_assistant/purchases/widgets/premium_carousel_purchases.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../utils/utils.dart';
import '../bloc/purchases_bloc.dart';
import '../widgets/purchases_appbar.dart';
import '../widgets/purchases_continue_button.dart';
import '../widgets/purchases_plan_selection.dart';
import '../widgets/restore_purchases.dart';

class PurchasesPage extends StatelessWidget {
  const PurchasesPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocListener<PurchasesBloc, PurchasesState>(
      listener: (context, state) {
        if (state.status == PurchasesStatus.fail) {
          bool isDialogOpen = _isThereCurrentDialogShowing(context);

          if (isDialogOpen) {
            Navigator.of(context).pop();
            switch (state.message) {
              case 'not-restored':
                Utils.showPurchaseDialog(
                    context,
                    AppLocalizations.of(context)!.sorry,
                    AppLocalizations.of(context)!.no_active_subscription,
                    false);

                break;
              default:
                Utils.showPurchaseDialog(
                    context,
                    AppLocalizations.of(context)!.sorry,
                    AppLocalizations.of(context)!.error_occured,
                    false);
            }
          }
        }
        if (state.status == PurchasesStatus.restored) {
          bool isDialogOpen = _isThereCurrentDialogShowing(context);

          if (isDialogOpen) {
            Navigator.of(context).pop();
            Utils.showPurchaseDialog(
                context,
                AppLocalizations.of(context)!.purchases_restored_success,
                AppLocalizations.of(context)!.purchases_restored,
                true);
          }
        }

        if (state.status == PurchasesStatus.purchased) {
          bool isDialogOpen = _isThereCurrentDialogShowing(context);

          if (isDialogOpen) {
            Navigator.of(context).pop();
            Utils.showPurchaseDialog(
                context,
                AppLocalizations.of(context)!.thank_you,
                AppLocalizations.of(context)!.thank_you_purchase,
                true);
          }
        }

        if (state.status == PurchasesStatus.loading) {
          Utils.showLoadingIndicator(
              context, AppLocalizations.of(context)!.loading);
        }

        if (state.status == PurchasesStatus.cancel) {
          bool isDialogOpen = _isThereCurrentDialogShowing(context);

          if (isDialogOpen) {
            Navigator.of(context).pop();
          }
        }
      },
      child: const Scaffold(
        body: CustomScrollView(
          slivers: <Widget>[
            PurchasesAppbar(),
            PremiumCarouselPurchases(),
            PurchasesPlanSelection(),
            CancelAnytimePurchases(),
            ContinueButton(),
            RestorePurchases()
          ],
        ),
      ),
    );
  }
}

_isThereCurrentDialogShowing(BuildContext context) =>
    ModalRoute.of(context)?.isCurrent != true;
