part of 'talk_bloc.dart';

//sealed
class TalkState extends Equatable {
  const TalkState();

  @override
  List<Object> get props => [];
}

class TalkInitial extends TalkState {}
