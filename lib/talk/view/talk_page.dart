import 'package:ai_assistant/circle_wave/circle_wave_painter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class TalkPage extends StatelessWidget {
  const TalkPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(AppLocalizations.of(context)!.talk)),
      body: Column(
        children: [
          const DemoCircleWave(
            key: Key('circle_wave'),
          ),
          Text(AppLocalizations.of(context)!.ask_something)
        ],
      ),
    );
  }
}
