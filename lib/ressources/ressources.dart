import 'package:flutter/material.dart';

class Ressources {
  List<Color> colors = [
    const Color.fromRGBO(220, 79, 0, 1),
    const Color.fromRGBO(69, 24, 219, 1),
    const Color.fromRGBO(103, 103, 103, 1),
    const Color.fromRGBO(255, 128, 108, 1),
    const Color.fromRGBO(123, 41, 255, 1),
    const Color.fromRGBO(39, 142, 255, 1),
    const Color.fromRGBO(98, 113, 255, 1),
    const Color.fromRGBO(255, 96, 145, 1),
    const Color.fromRGBO(81, 39, 221, 1),
    const Color.fromRGBO(255, 128, 108, 1),
    const Color.fromRGBO(255, 92, 0, 1),
    const Color.fromRGBO(167, 255, 229, 1),
    const Color.fromRGBO(0, 207, 195, 1),
    const Color.fromRGBO(0, 0, 0, 1),
    const Color.fromRGBO(232, 11, 11, 1),
    const Color.fromRGBO(255, 92, 0, 1),
  ];
}
