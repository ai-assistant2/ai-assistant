import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:rate_my_app/rate_my_app.dart';

import '../bloc/chat_bloc.dart';
import '../widgets/chat_appbar.dart';
import '../widgets/chat_bar.dart';
import '../widgets/chat_list.dart';

class ChatPage extends StatefulWidget {
  final bool autofocus;

  const ChatPage({
    super.key,
    required this.autofocus,
  });

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final FocusNode _focusNode = FocusNode();

  @override
  void dispose() {
    _focusNode.unfocus();
    super.dispose();
  }

  RateMyApp rateMyApp = RateMyApp(
    preferencesPrefix: 'rateMyApp_',
    minDays: 3, // Show rate popup on first day of install.
    minLaunches: 0, //
    googlePlayIdentifier: 'com.kairos.ai_assistant',
    appStoreIdentifier: '1668871275',
  );

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => SystemChannels.textInput.invokeMethod('TextInput.hide'),
      child: Scaffold(
        key: _scaffoldKey,
        appBar: const PreferredSize(
          preferredSize: Size.fromHeight(80),
          child: ChatAppbar(),
        ),
        body: BlocListener<ChatBloc, ChatState>(
          listener: (context, state) async {
            if (state.chatActionStatus == ChatActionStatus.requestReview) {
              rateMyApp.init().then((_) {
                if (rateMyApp.shouldOpenDialog) {
                  rateMyApp.showRateDialog(
                    context,
                    title: AppLocalizations.of(context)!.rate_us,
                    message: AppLocalizations.of(context)!.rate_us_description,
                    rateButton: AppLocalizations.of(context)!
                        .rate_button, // The dialog "rate" button text.
                    noButton: AppLocalizations.of(context)!
                        .no_rate_button, // The dialog "no" button text.
                    laterButton: AppLocalizations.of(context)!
                        .maybe_later, // The dialog "later" button text.
                    listener: (button) {
                      context
                          .read<ChatBloc>()
                          .add(RateButtonClicked(rate: button));
                      // The button click listener (useful if you want to cancel the click event).

                      return true; // Return false if you want to cancel the click event.
                    },
                    ignoreNativeDialog: Platform
                        .isAndroid, // Set to false if you want to show the Apple's native app rating dialog on iOS or Google's native app rating dialog (depends on the current Platform).
                    dialogStyle: const DialogStyle(), // Custom dialog styles.
                    onDismissed: () => rateMyApp.callEvent(RateMyAppEventType
                        .laterButtonPressed), // Called when the user dismissed the dialog (either by taping outside or by pressing the "back" button).
                    // contentBuilder: (context, defaultContent) => content, // This one allows you to change the default dialog content.
                    // actionsBuilder: (context) => [], // This one allows you to use your own buttons.
                  );
                } else {
                  debugPrint('not asking for rating');
                }
              });
            }
          },
          child: Stack(
            children: [
              ChatList(
                focusNode: _focusNode,
              ),
              ChatBar(
                autofocus: widget.autofocus,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
