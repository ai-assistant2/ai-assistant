import 'package:ai_assistant/ocr/views/ocr_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../bloc/chat_bloc.dart';

class ChatBar extends StatefulWidget {
  final bool autofocus;

  const ChatBar({
    required this.autofocus,
    super.key,
  });

  @override
  State<ChatBar> createState() => _ChatBarState();
}

class _ChatBarState extends State<ChatBar> {
  @override
  Widget build(BuildContext context) {
    final FocusNode focusNode = FocusNode();

    return BlocBuilder<ChatBloc, ChatState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            decoration: BoxDecoration(
                color: Theme.of(context).appBarTheme.backgroundColor,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20))),
            child: Container(
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              child: SizedBox(
                  height: 100.0,
                  width: MediaQuery.of(context).size.width,
                  child: state.status == ChatStatus.loadingChat
                      ? Center(
                          child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  Theme.of(context).primaryColor)))
                      : Form(
                          child: CupertinoTextField(
                            key: const Key('chat_textfield'),
                            controller:
                                context.read<ChatBloc>().inputController,
                            focusNode: focusNode,
                            autofocus: false,
                            cursorColor: Theme.of(context).primaryColor,
                            maxLines: 5,
                            padding: const EdgeInsets.only(
                                left: 20, bottom: 5, top: 5),
                            style: Theme.of(context).textTheme.bodyLarge,
                            textInputAction: TextInputAction.send,
                            onSubmitted: (s) {
                              if (state.status != ChatStatus.loading) {
                                context
                                    .read<ChatBloc>()
                                    .add(ChatStarted(question: s));

                                context
                                            .read<ChatBloc>()
                                            .inputController
                                            .text
                                            .length >
                                        1
                                    ? context
                                        .read<ChatBloc>()
                                        .inputController
                                        .clear()
                                    : null;
                              }
                            },
                            onChanged: (value) {
                              context
                                  .read<ChatBloc>()
                                  .add(TextfieldUpdated(question: value));
                            },
                            prefix: OcrButton(),
                            placeholder:
                                AppLocalizations.of(context)!.ask_something,
                            placeholderStyle: TextStyle(
                              fontWeight: Theme.of(context)
                                  .textTheme
                                  .bodySmall
                                  ?.fontWeight,
                              color: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.color
                                  ?.withOpacity(0.7),
                            ),
                            suffix: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(0, 6.0, .0, 6.0),
                                child: IconButton(
                                    onPressed: () {
                                      if (state.status != ChatStatus.loading) {
                                        context.read<ChatBloc>().add(
                                            ChatStarted(
                                                question: context
                                                    .read<ChatBloc>()
                                                    .inputController
                                                    .text));

                                        context
                                                    .read<ChatBloc>()
                                                    .inputController
                                                    .text
                                                    .length >
                                                1
                                            ? context
                                                .read<ChatBloc>()
                                                .inputController
                                                .clear()
                                            : null;
                                      }
                                    },
                                    icon: const Icon(Icons.send),
                                    color: Theme.of(context).primaryColor)),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20.0),
                              color:
                                  Theme.of(context).appBarTheme.backgroundColor,
                            ),
                          ),
                        )),
            ),
          ),
        );
      },
    );
  }
}
