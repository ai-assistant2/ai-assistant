
import 'package:ai_assistant/chat/bloc/chat_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';

class ChatAppbar extends StatelessWidget {
  const ChatAppbar({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatBloc, ChatState>(
      builder: (context, state) {
        return AppBar(
          elevation: 0.0,
          centerTitle: false,
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          title: state.status == ChatStatus.ready
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(AppLocalizations.of(context)!.new_chat,
                        style:
                            Theme.of(context).textTheme.headlineSmall?.copyWith(
                                  color: Theme.of(context).primaryColor,
                                  fontWeight: FontWeight.bold,
                                )),
                    Text(
                      formatDate(DateTime.fromMillisecondsSinceEpoch(
                        state.chatMessageList.last.dtCreated,
                      )), // AppLocalizations.of(context).new,
                    ),
                  ],
                )
              : Text(
                  AppLocalizations.of(context)!.new_chat,
                  style: Theme.of(context).textTheme.headlineSmall?.copyWith(
                        color: Theme.of(context).primaryColor,
                      ),
                ),
        );
      },
    );
  }

  String formatDate(DateTime date) {
    final formatter = DateFormat('HH:mm • MMMM dd, yyyy').format(date);

    return formatter;
  }

  /*

  FlexibleSpaceBar(
            background: isTransparentAppbar
                ? PreferredSize(
                    preferredSize: Size(MediaQuery.of(context).size.width, 22),
                    child: ClipRRect(
                        child: BackdropFilter(
                            filter: ImageFilter.blur(sigmaX: 20, sigmaY: 50),
                            child: Container(
                                width: MediaQuery.of(context).size.width,
                                height: MediaQuery.of(context).padding.top,
                                color: Colors.transparent.withOpacity(0.1)))),
                  )
                : const SizedBox.shrink(),
          ),*/
}
