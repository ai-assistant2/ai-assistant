import 'package:ai_assistant/model/chat_message.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../ads/bloc/ads_bloc.dart';
import '../bloc/chat_bloc.dart';
import 'no_chat_message.dart';

class ChatList extends StatefulWidget {
  final FocusNode focusNode;

  const ChatList({
    super.key,
    required this.focusNode,
  });

  @override
  State<ChatList> createState() => _ChatListState();
}

class _ChatListState extends State<ChatList> {
  ScrollController scrollController = ScrollController(
      initialScrollOffset: 1500); //TODO FIX THE SCROLL CONTROLLER TO THE END

  @override
  void initState() {
    super.initState();
    scrollController.addListener(_onScroll);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ChatBloc, ChatState>(
      listener: (context, state) {
        if (state.chatActionStatus == ChatActionStatus.updatedAnswer) {
          _animateToLast(scrollController);
          context.read<AdsBloc>().add(AdsShowed());
        }

        if (state.chatActionStatus == ChatActionStatus.toLastElement) {
          _animateToLast(scrollController);
        }

        if (state.chatActionStatus == ChatActionStatus.fail) {
          Fluttertoast.showToast(
              msg: AppLocalizations.of(context)!.error_occured,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.TOP,
              timeInSecForIosWeb: 10,
              backgroundColor: Theme.of(context).primaryColor,
              textColor: Colors.white,
              fontSize: 16.0);
        }
        if (state.chatActionStatus == ChatActionStatus.textTooShort) {
          Fluttertoast.showToast(
              msg: AppLocalizations.of(context)!.too_short_text,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.TOP,
              timeInSecForIosWeb: 3,
              backgroundColor: Theme.of(context).primaryColor,
              textColor: Colors.white,
              fontSize: 16.0);
        }

        if (state.chatActionStatus == ChatActionStatus.copiedToClipboard) {
          Fluttertoast.showToast(
              msg: AppLocalizations.of(context)!.copied_to_clipboard,
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.TOP,
              timeInSecForIosWeb: 1,
              backgroundColor: Theme.of(context).primaryColor,
              textColor: Colors.white,
              fontSize: 16.0);
        }
      },
      builder: (context, state) {
        switch (state.status) {
          case ChatStatus.emptyList:
            return const NoChatMessage();

          case ChatStatus.loading:
          case ChatStatus.initial:
            return const Center(child: CircularProgressIndicator());

          case ChatStatus.ready:
          default:
            return GestureDetector(
              onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
              child: ListView.builder(
                  padding: const EdgeInsets.only(bottom: 120),
                  itemBuilder: (context, index) {
                    return index == 0 && !state.hasReachedMax
                        ? const _TestLoader()
                        : MessageTile(
                            chatMessage: state.chatMessageList[index],
                            focusNode: widget.focusNode,
                          );
                  },
                  controller: scrollController,
                  itemCount: state.chatMessageList.length),
            );
        }
      },
    );
  }

  @override
  void dispose() {
    if (scrollController.hasClients) {
      scrollController.dispose();
    }

    super.dispose();
  }

  void _onScroll() {
    // if (_isTop) context.read<ChatBloc>().add(const UserDataLoaded());
  }

  Future _animateToLast(ScrollController scrollController) async {
    if (scrollController.hasClients) {
      scrollController.position.pixels <
              scrollController.position.maxScrollExtent
          ? scrollController.animateTo(
              scrollController.position.maxScrollExtent,
              duration: const Duration(seconds: 2),
              curve: Curves.easeOut)
          : null;
    }
  }

  _isThereCurrentDialogShowing(BuildContext context) =>
      ModalRoute.of(context)?.isCurrent != true;
}

class MessageTile extends StatelessWidget {
  final ChatMessage chatMessage;

  final FocusNode focusNode;
  const MessageTile({
    super.key,
    required this.focusNode,
    required this.chatMessage,
  });

  @override
  Widget build(BuildContext context) {
    bool isMine = chatMessage.role == 'user';

    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(0.0),
        ),
        margin: EdgeInsets.zero,
        color: Theme.of(context).scaffoldBackgroundColor,
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Icon(
                        CupertinoIcons.circle_fill,
                        color: isMine
                            ? Theme.of(context).colorScheme.secondary
                            : Theme.of(context).primaryColor,
                      ),
                      const SizedBox(
                        width: 5,
                      ),
                      Text(isMine
                          ? AppLocalizations.of(context)!.you
                          : AppLocalizations.of(context)!.iris),
                    ],
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: IconButton(
                        onPressed: () => context
                            .read<ChatBloc>()
                            .add(CopiedToClipboard(text: chatMessage.answer)),
                        icon: Icon(
                          CupertinoIcons.doc_on_clipboard,
                          color: isMine
                              ? Theme.of(context).colorScheme.secondary
                              : Theme.of(context).primaryColor,
                        )),
                  ),
                ],
              ),
            ),
            Padding(
                padding: const EdgeInsets.only(
                    top: 50, left: 20, right: 15, bottom: 15),
                child: chatMessage.answer.contains('```')
                    ? Markdown(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        padding: EdgeInsets.zero,
                        softLineBreak: true,
                        styleSheet:
                            MarkdownStyleSheet.fromTheme(Theme.of(context))
                                .copyWith(
                                    code: TextStyle(
                                      fontSize: Theme.of(context)
                                          .textTheme
                                          .titleMedium
                                          ?.fontSize,
                                      backgroundColor: Theme.of(context)
                                          .colorScheme
                                          .inversePrimary,
                                    ),
                                    codeblockPadding: const EdgeInsets.only(
                                        bottom: 60,
                                        top: 30,
                                        left: 15,
                                        right: 15),
                                    p: Theme.of(context).textTheme.titleLarge,
                                    codeblockDecoration: BoxDecoration(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .inversePrimary,
                                        borderRadius: const BorderRadius.all(
                                            Radius.circular(20))),
                                    textAlign: WrapAlignment.spaceAround),
                        selectable: true,
                        data: chatMessage.answer,
                      )
                    : SelectableText(
                        isMine
                            ? chatMessage.answer.capitalize()
                            : chatMessage.answer,
                        // focusNode: focusNode,
                        textAlign: TextAlign.justify,
                        style: Theme.of(context).textTheme.titleLarge?.copyWith(
                            color:
                                Theme.of(context).textTheme.bodyLarge!.color)))
          ],
        ));
  }
}

extension StringExtension on String {
  String capitalize() {
    if (isEmpty) {
      return this;
    }
    return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  }
}

class _TestLoader extends StatelessWidget {
  const _TestLoader();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatBloc, ChatState>(
      builder: (context, state) {
        return Align(
          alignment: Alignment.center,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: CupertinoButton(
                color: Theme.of(context).primaryColor,
                onPressed: () =>
                    context.read<ChatBloc>().add(const UserDataLoaded()),
                child: Text(
                  AppLocalizations.of(context)!.load_previous,
                  style: Theme.of(context)
                      .textTheme
                      .bodyLarge
                      ?.copyWith(color: CupertinoColors.white),
                )),
          ),
        );
      },
    );
  }
}
