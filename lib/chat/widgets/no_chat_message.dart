import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../bloc/chat_bloc.dart';

class NoChatMessage extends StatelessWidget {
  const NoChatMessage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatBloc, ChatState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25.0),
          child: SingleChildScrollView(
            padding: EdgeInsets.zero,
            child: Center(
              child: Column(
                children: [
                  Container(
                    width: 150,
                    height: 150,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: AssetImage('assets/images/iris_profile.jpg'),
                          fit: BoxFit.cover),
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    AppLocalizations.of(context)!.ask_me_anything,
                    style: Theme.of(context).textTheme.headlineMedium,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Icon(
                    CupertinoIcons.lab_flask,
                    size: 40,
                  ),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(AppLocalizations.of(context)!
                          .explain_quantum_physics),
                    ),
                  ),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                          AppLocalizations.of(context)!.how_to_make_request),
                    ),
                  ),
                  const Icon(
                    CupertinoIcons.device_laptop,
                    size: 40,
                  ),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(AppLocalizations.of(context)!
                          .when_built_eiffel_tower),
                    ),
                  ),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Text(
                          AppLocalizations.of(context)!.solve_math_problem),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
