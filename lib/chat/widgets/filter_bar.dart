import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../bloc/chat_bloc.dart';

class FilterBar extends StatefulWidget {
  const FilterBar({super.key});

  @override
  State<FilterBar> createState() => _FilterBarState();
}

class _FilterBarState extends State<FilterBar> {
  final TextEditingController _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatBloc, ChatState>(
      builder: (context, state) {
        return SliverAppBar(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(10),
            ),
          ),
          title: SizedBox(
            height: 80.0,
            width: double.infinity,
            child: CupertinoTextField(
              autofocus: true,
              suffix: Material(
                color: Colors.transparent,
                child: TextButton(
                  child: Text(AppLocalizations.of(context)!.cancel),
                  onPressed: () => context
                      .read<ChatBloc>()
                      .add(const FilterBarHided(showFilterBar: false)),
                ),
              ),
              maxLines: 1,
              controller: _controller,
              textInputAction: TextInputAction.done,
              onChanged: (value) {
                if (_controller.text.length >= 2) {
                  context
                      .read<ChatBloc>()
                      .add(FilteredSearchUpdated(searchText: value));
                }
              },
              prefix: const Padding(
                  padding: EdgeInsets.fromLTRB(9.0, 6.0, 9.0, 6.0),
                  child: Icon(CupertinoIcons.search)),
              maxLength: 60,
              placeholderStyle: const TextStyle(
                color: Color.fromARGB(255, 89, 89, 90),
              ),
              placeholder: AppLocalizations.of(context)!.search,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: const Color(0xffF0F1F5),
              ),
            ),
          ),
        );
      },
    );
  }
}


// search for similar words  if similar create a list that unit the index,
