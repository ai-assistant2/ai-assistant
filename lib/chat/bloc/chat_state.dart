part of 'chat_bloc.dart';

enum ChatStatus {
  initial,
  loading,
  emptyList,
  ready,
  loadingChat,
  fail,
}

enum ChatActionStatus {
  initial,
  loading,
  copiedToClipboard,
  messageShared,
  fail,
  messageFail,
  textfieldEditing,
  textTooShort,
  requestReview,
  updatedAnswer,
  filteringSearch,
  toLastElement
}

class ChatState extends Equatable {
  const ChatState(
      {this.chatMessageList = const [],
      this.isSpeakerActive = false,
      this.status = ChatStatus.initial,
      this.textfieldValue = '',
      this.hideInfo = false,
      this.chatID = '',
      this.errorMessage = '',
      this.filteredIndexes = const [],
      this.filteredValue = '',
      this.userID = '',
      this.showFilterBar = false,
      this.sharedMessage = '',
      this.hasReachedMax = false,
      this.chatActionStatus = ChatActionStatus.initial});

  final List<ChatMessage> chatMessageList;
  final bool isSpeakerActive;
  final ChatStatus status;
  final String textfieldValue;
  final bool hideInfo;
  final String chatID;
  final String userID;
  final String errorMessage;
  final bool showFilterBar;
  final List<int> filteredIndexes;
  final String filteredValue;
  final String sharedMessage;
  final ChatActionStatus chatActionStatus;
  final bool hasReachedMax;

  ChatState copyWith(
      {ChatStatus? status,
      List<ChatMessage>? chatMessageList,
      String? errorMessage,
      bool? isSpeakerActive,
      String? textfieldValue,
      bool? hideInfo,
      String? chatID,
      List<int>? filteredIndexes,
      String? filteredValue,
      String? userID,
      bool? showFilterBar,
      String? sharedMessage,
      bool? hasReachedMax,
      ChatActionStatus? chatActionStatus}) {
    return ChatState(
        status: status ?? this.status,
        chatActionStatus: chatActionStatus ?? this.chatActionStatus,
        errorMessage: errorMessage ?? this.errorMessage,
        chatMessageList: chatMessageList ?? this.chatMessageList,
        isSpeakerActive: isSpeakerActive ?? this.isSpeakerActive,
        textfieldValue: textfieldValue ?? this.textfieldValue,
        hideInfo: hideInfo ?? this.hideInfo,
        filteredValue: filteredValue ?? this.filteredValue,
        filteredIndexes: filteredIndexes ?? this.filteredIndexes,
        chatID: chatID ?? this.chatID,
        userID: userID ?? this.userID,
        showFilterBar: showFilterBar ?? this.showFilterBar,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        sharedMessage: sharedMessage ?? this.sharedMessage);
  }

  @override
  List<Object?> get props => [
        chatMessageList,
        errorMessage,
        isSpeakerActive,
        status,
        textfieldValue,
        hideInfo,
        chatID,
        userID,
        filteredIndexes,
        filteredValue,
        showFilterBar,
        sharedMessage,
        chatActionStatus,
        hasReachedMax
      ];
}
