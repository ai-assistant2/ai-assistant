import 'package:ai_assistant/model/chat_message.dart';
import 'package:ai_assistant/model/gpt_response.dart';
import 'package:ai_assistant/repository/api_request.dart';
import 'package:ai_assistant/repository/chat_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rate_my_app/rate_my_app.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'chat_event.dart';
part 'chat_state.dart';

class ChatBloc extends Bloc<ChatEvent, ChatState> {
  List<ChatMessage> chatMessageList = [];
  List<int> filteredIndexes = [];
  String? convID;
  //late SharedPreferences? _prefs;
  late String? _userID;

  int maxFailedLoadAttempts = 3;
  final ChatRepository _chatRepository;
  final inputController = TextEditingController();

  ChatBloc({
    required this.convID,
    required ChatRepository chatRepository,
  })  : _chatRepository = chatRepository,
        super(const ChatState()) {
    on<UserDataLoaded>(_onUserDataLoaded);
    on<ChatStarted>(_onChatStarted);
    on<SpeakerUpdated>(_onSpeakerUpdated);
    on<TextfieldUpdated>(_onTextfieldUpdated);
    on<HidedInfo>(_onHidedInfo);
    on<FilterBarHided>(_onFilterBarHided);
    on<FilteredSearchUpdated>(_onFilteredSearchUpdated);
    on<CopiedToClipboard>(_onCopiedToClipBoard);
    on<MessageShared>(_onMessageShared);
    on<RateButtonClicked>(_onRateButtonClicked);
  }

  Future _onRateButtonClicked(
      RateButtonClicked event, Emitter<ChatState> emit) async {
    if (event.rate == RateMyAppDialogButton.rate) {
      SharedPreferences prefs = await SharedPreferences.getInstance();

      prefs.setBool('alreadyRated', true);
    }
  }

  Future _onUserDataLoaded(
      UserDataLoaded event, Emitter<ChatState> emit) async {
    if (state.hasReachedMax) return;

    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs = await SharedPreferences.getInstance();

    _userID = prefs.getString('userID');

    if (convID != null) {
      if (state.status == ChatStatus.initial) {
        final chatList = await _chatRepository.getConv(convID!, null);
        emit(state.copyWith(
            hasReachedMax: false,
            userID: _userID,
            chatMessageList: chatList,
            chatActionStatus: ChatActionStatus.toLastElement,
            status: ChatStatus.ready));
      } else {
        final chatList = await _chatRepository.getConv(
            convID!, state.chatMessageList.first.dtCreated);

        chatList.isEmpty
            ? emit(state.copyWith(
                hasReachedMax: true,
                status: ChatStatus.ready,
                chatActionStatus: ChatActionStatus.initial))
            : emit(state.copyWith(
                hasReachedMax: false,
                userID: _userID,
                chatMessageList: List.of(state.chatMessageList)
                  ..insertAll(0, chatList),
                chatActionStatus: ChatActionStatus.initial,
                status: ChatStatus.ready));
      }
    } else {
      emit(state.copyWith(
          userID: _userID,
          hasReachedMax: true,
          chatActionStatus: ChatActionStatus.initial,
          status: _userID != '' ? ChatStatus.emptyList : ChatStatus.fail));
    }
  }

  Future _onChatStarted(
    ChatStarted event,
    Emitter<ChatState> emit,
  ) async {
    try {
      if (event.question.length <= 1) {
        emit(state.copyWith(
          chatActionStatus: ChatActionStatus.loading,
        ));
        emit(state.copyWith(chatActionStatus: ChatActionStatus.textTooShort));
      } else {
        if (state.chatMessageList.isNotEmpty &&
            state.chatMessageList.last.hasError) {
          emit(state.copyWith(
            status: ChatStatus.loadingChat,
          ));
        } else {
          emit(state.copyWith(
            status: ChatStatus.loadingChat,
          ));
          convID == null
              ? convID = await _chatRepository.createConv(
                  ChatMessage(
                    answer: state.textfieldValue,
                    dtCreated: DateTime.now().millisecondsSinceEpoch,
                    role: 'user',
                    hasError: false,
                    userID: _userID ?? '1234',
                  ),
                  'title',
                  _userID ?? '1234')
              : await _chatRepository.updateConv(
                  ChatMessage(
                    answer: state.textfieldValue,
                    dtCreated: DateTime.now().millisecondsSinceEpoch,
                    role: 'user',
                    hasError: false,
                    userID: _userID ?? '1234',
                  ),
                  convID!,
                );

          emit(state.copyWith(
              status: ChatStatus.loadingChat,
              chatMessageList: List.of(state.chatMessageList)
                ..add(ChatMessage(
                    hasError: false,
                    answer: state.textfieldValue,
                    dtCreated: DateTime.now().millisecondsSinceEpoch,
                    role: 'user',
                    userID: _userID ?? '1234'))));
        }

        GptResponse gptResponse = await HttpRequest.askChat(
          state.textfieldValue,
        );

        debugPrint('convID IS $convID');

        convID == null
            ? convID = await _chatRepository.createConv(
                ChatMessage(
                  answer: gptResponse.result ?? '',
                  dtCreated: DateTime.now().millisecondsSinceEpoch,
                  role: 'assistant',
                  hasError: false,
                  userID: _userID ?? '1234',
                ),
                'title',
                _userID ?? '1234')
            : await _chatRepository.updateConv(
                ChatMessage(
                  answer: gptResponse.result ?? '',
                  dtCreated: DateTime.now().millisecondsSinceEpoch,
                  role: 'assistant',
                  hasError: false,
                  userID: _userID ?? '1234',
                ),
                convID!,
              );

        emit(state.copyWith(
            chatMessageList: List.of(state.chatMessageList)
              ..add(ChatMessage(
                  hasError: false,
                  answer: gptResponse.result ?? '',
                  dtCreated: DateTime.now().millisecondsSinceEpoch,
                  role: 'assistant',
                  userID: state.userID)),
            chatActionStatus: ChatActionStatus.updatedAnswer,
            status: ChatStatus.ready));
      }
    } catch (e) {
      state.chatMessageList.isNotEmpty
          ? state.chatMessageList.last.hasError = true
          : null;
      emit(state.copyWith(
          chatActionStatus: ChatActionStatus.fail,
          status: ChatStatus.fail,
          chatMessageList: state.chatMessageList));
    }
  }

  Future _onCopiedToClipBoard(
      CopiedToClipboard event, Emitter<ChatState> emit) async {
    emit(state.copyWith(chatActionStatus: ChatActionStatus.loading));
    await Clipboard.setData(ClipboardData(text: event.text));
    emit(state.copyWith(chatActionStatus: ChatActionStatus.copiedToClipboard));
  }

  Future _onTextfieldUpdated(
      TextfieldUpdated event, Emitter<ChatState> emit) async {
    inputController.text = event.question;

    emit(state.copyWith(
        textfieldValue: event.question,
        chatActionStatus: ChatActionStatus.textfieldEditing));
  }

  Future _onHidedInfo(HidedInfo event, Emitter<ChatState> emit) async {
    emit(state.copyWith(hideInfo: true));
  }

  Future _onMessageShared(MessageShared event, Emitter<ChatState> emit) async {
    emit(state.copyWith(chatActionStatus: ChatActionStatus.loading));
    emit(state.copyWith(
        sharedMessage: event.text,
        chatActionStatus: ChatActionStatus.messageShared));
  }

  Future _onSpeakerUpdated(
      SpeakerUpdated event, Emitter<ChatState> emit) async {
    emit(state.copyWith(isSpeakerActive: event.isActive));
  }

  Future _onFilteredSearchUpdated(
      FilteredSearchUpdated event, Emitter<ChatState> emit) async {
    filteredIndexes.clear();

    for (int i = 0; i < state.chatMessageList.length; i++) {
      String searchedText = event.searchText;
      if (state.chatMessageList[i].answer.contains(searchedText)) {
        filteredIndexes.add(i);
      }
    }
    if (filteredIndexes.isNotEmpty) {
      emit(state.copyWith(
          chatActionStatus: ChatActionStatus.filteringSearch,
          filteredIndexes: filteredIndexes,
          filteredValue: event.searchText));
    }
  }

  Future _onFilterBarHided(
      FilterBarHided event, Emitter<ChatState> emit) async {
    emit(state.copyWith(showFilterBar: event.showFilterBar));
  }

  Future checkIfRated() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    bool alreadyRated =
        prefs != null ? prefs.containsKey('alreadyRated') : false;
    return alreadyRated;
  }
}
