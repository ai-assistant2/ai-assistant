part of 'chat_bloc.dart';

abstract class ChatEvent extends Equatable {
  const ChatEvent();

  @override
  List<Object> get props => [];
}

class SpeakerUpdated extends ChatEvent {
  final bool isActive;
  const SpeakerUpdated({required this.isActive});
}

class CopiedToClipboard extends ChatEvent {
  final String text;
  const CopiedToClipboard({required this.text});
}

class ChatStarted extends ChatEvent {
  final String question;
  const ChatStarted({required this.question});
}

class HidedInfo extends ChatEvent {
  final bool hidedInfo;
  const HidedInfo({required this.hidedInfo});
}

class TextfieldUpdated extends ChatEvent {
  final String question;
  const TextfieldUpdated({required this.question});
}

class UserDataLoaded extends ChatEvent {
  const UserDataLoaded();
}

class FilteredSearchUpdated extends ChatEvent {
  final String searchText;

  const FilteredSearchUpdated({
    required this.searchText,
  });
}

class MessageShared extends ChatEvent {
  final String text;
  const MessageShared({required this.text});
}

class RateButtonClicked extends ChatEvent {
  final RateMyAppDialogButton rate;
  const RateButtonClicked({required this.rate});
}

class FilterBarHided extends ChatEvent {
  final bool showFilterBar;
  const FilterBarHided({required this.showFilterBar});
}
