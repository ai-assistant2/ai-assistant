part of 'login_bloc.dart';

enum SignInMethod { apple, google, email, anonymous, signUp }

class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object> get props => [];
}

class LoginSubmittedWith3rdParty extends LoginEvent {
  final SignInMethod signInMethod;

  const LoginSubmittedWith3rdParty({required this.signInMethod});
}

class LoginEmailChanged extends LoginEvent {
  const LoginEmailChanged(this.email);

  final String email;

  @override
  List<Object> get props => [email];
}

class LoginPasswordChanged extends LoginEvent {
  const LoginPasswordChanged(this.password);

  final String password;

  @override
  List<Object> get props => [password];
}

class SignUp extends LoginEvent {}

class LoginSubmitted extends LoginEvent {
  final SignInMethod signInMethod;
  const LoginSubmitted({required this.signInMethod});
}

class SignChanged extends LoginEvent {
  final int method;
  const SignChanged({required this.method});
}
