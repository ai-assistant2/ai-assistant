part of 'login_bloc.dart';

enum LoginStatus { signIn, signUp, loading, initial, success, fail }

class LoginState extends Equatable {
  const LoginState(
      {this.formStatus = FormzSubmissionStatus.initial,
      this.email = const Email.pure(),
      this.password = const Password.pure(),
      this.isValid = false,
      this.status = LoginStatus.signIn,
      this.authResult});

  final FormzSubmissionStatus formStatus;
  final Email email;
  final Password password;
  final bool isValid;
  final LoginStatus status;
  final AuthResult? authResult;

  LoginState copyWith(
      {FormzSubmissionStatus? formStatus,
      Email? email,
      Password? password,
      bool? isValid,
      LoginStatus? status,
      AuthResult? authResult}) {
    return LoginState(
        formStatus: formStatus ?? this.formStatus,
        email: email ?? this.email,
        password: password ?? this.password,
        isValid: isValid ?? this.isValid,
        status: status ?? this.status,
        authResult: authResult ?? this.authResult);
  }

  @override
  List<Object?> get props => [formStatus, email, password, status, authResult];
}
