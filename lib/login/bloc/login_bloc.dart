import 'package:ai_assistant/model/auth_result.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';

import '../../model/email.dart';
import '../../model/password.dart';
import '../../repository/auth_repository.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthRepository _authenticationRepository;
  LoginBloc({
    required AuthRepository authenticationRepository,
  })  : _authenticationRepository = authenticationRepository,
        super(const LoginState()) {
    on<LoginEmailChanged>(_onEmailChanged);
    on<LoginPasswordChanged>(_onPasswordChanged);
    on<LoginSubmitted>(_onSubmitted);
    on<SignChanged>(_onSignChanged);
    on<LoginSubmittedWith3rdParty>(_onLoginSubmittedWith3rdParty);
  }

  void _onSignChanged(SignChanged event, Emitter<LoginState> emit) {
    if (event.method == 0) {
      emit(state.copyWith(status: LoginStatus.signIn));
    } else {
      emit(state.copyWith(status: LoginStatus.signUp));
    }
  }

  void _onEmailChanged(
    LoginEmailChanged event,
    Emitter<LoginState> emit,
  ) {
    final email = Email.dirty(event.email);
    emit(
      state.copyWith(
        email: email,
        isValid: Formz.validate([state.password, email]),
      ),
    );
  }

  void _onPasswordChanged(
    LoginPasswordChanged event,
    Emitter<LoginState> emit,
  ) {
    final password = Password.dirty(event.password);
    emit(
      state.copyWith(
        password: password,
        isValid: Formz.validate([password, state.email]),
      ),
    );
  }

  Future<void> _onLoginSubmittedWith3rdParty(
    LoginSubmittedWith3rdParty event,
    Emitter<LoginState> emit,
  ) async {
    emit(state.copyWith(status: LoginStatus.loading));
    switch (event.signInMethod) {
      case SignInMethod.anonymous:
        AuthResult authResult =
            await _authenticationRepository.signInAnonymously();
        authResult.user != null
            ? emit(state.copyWith(
                status: LoginStatus.success, authResult: authResult))
            : emit(state.copyWith(
                status: LoginStatus.fail, authResult: authResult));

        break;

      case SignInMethod.apple:
        AuthResult authResult =
            await _authenticationRepository.signInWithApple();

        authResult.user != null
            ? emit(state.copyWith(
                status: LoginStatus.success, authResult: authResult))
            : emit(state.copyWith(
                status: LoginStatus.fail, authResult: authResult));

        break;

      case SignInMethod.google:
      default:
        AuthResult authResult =
            await _authenticationRepository.signInWithGoogle();

        authResult.user != null
            ? emit(state.copyWith(
                status: LoginStatus.success, authResult: authResult))
            : emit(state.copyWith(
                status: LoginStatus.fail, authResult: authResult));
    }
  }

  Future<void> _onSubmitted(
    LoginSubmitted event,
    Emitter<LoginState> emit,
  ) async {
    emit(state.copyWith(
        formStatus: FormzSubmissionStatus.inProgress,
        status: LoginStatus.loading));
    if (event.signInMethod == SignInMethod.signUp) {
      AuthResult authResult = await _authenticationRepository.signUpWithEmail(
          email: state.email.value, password: state.password.value);

      print(authResult.message);
      print(authResult.user);

      authResult.user != null
          ? emit(state.copyWith(
              formStatus: FormzSubmissionStatus.success,
              status: LoginStatus.success,
              authResult: authResult,
            ))
          : emit(state.copyWith(
              authResult: authResult,
              formStatus: FormzSubmissionStatus.canceled,
              status: LoginStatus.fail,
            ));
    } else {
      AuthResult authResult = await _authenticationRepository.signInWithEmail(
          email: state.email.value, password: state.password.value);

      authResult.user != null
          ? emit(state.copyWith(
              formStatus: FormzSubmissionStatus.success,
              status: LoginStatus.success,
              authResult: authResult,
            ))
          : emit(state.copyWith(
              authResult: authResult,
              status: LoginStatus.fail,
              formStatus: FormzSubmissionStatus.failure));
    }
  }
}
