import 'package:ai_assistant/repository/auth_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ForgetPasswordPage extends StatefulWidget {
  const ForgetPasswordPage({super.key});

  @override
  State<ForgetPasswordPage> createState() => _ForgetPasswordPageState();
}

class _ForgetPasswordPageState extends State<ForgetPasswordPage> {
  TextEditingController textEditingController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: 0.0,
          title: Text(
            AppLocalizations.of(context)!.forgot_password,
            style: Theme.of(context).textTheme.headlineSmall,
          )),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            const _EnterYourMail(),
            Form(
              key: _formKey,
              child: TextFormField(
                // key: const Key('loginForm_resetPasswordInput_textField'),
                onChanged: (value) {
                  setState(() {
                    textEditingController.text = value;
                  });
                },
                decoration: InputDecoration(
                  labelText: AppLocalizations.of(context)!.email,
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return AppLocalizations.of(context)!.invalid_email;
                  }
                  return null;
                },
              ),
            ),
            submitEmail(context, textEditingController.text)
          ],
        ),
      ),
    );
  }

  void _showAlertForgetPasswordDialog(
      BuildContext context, String title, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text(AppLocalizations.of(context)!.ok),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget submitEmail(BuildContext context, String email) {
    return InkWell(
      onTap: () => AuthRepository().resetPassword(email).then(
        (message) {
          if (_formKey.currentState!.validate()) {
            if (message == 'Password reset email sent') {
              _showAlertForgetPasswordDialog(
                  context,
                  AppLocalizations.of(context)!.great,
                  AppLocalizations.of(context)!.reset_password_sent);
            }
            if (message == 'user-not-found') {
              _showAlertForgetPasswordDialog(
                  context,
                  AppLocalizations.of(context)!.oups,
                  AppLocalizations.of(context)!.user_not_found);

              // When the user does not exist
            }

            if (message == 'invalid-email') {
              _showAlertForgetPasswordDialog(
                  context,
                  AppLocalizations.of(context)!.oups,
                  AppLocalizations.of(context)!.invalid_email);

              // When the email is not valid
            }
          }
        },
      ),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Center(
            child: Text(
              AppLocalizations.of(context)!.continue_button,
              style: Theme.of(context).textTheme.titleLarge,
            ),
          ),
        ),
      ),
    );
  }
}

class _EnterYourMail extends StatelessWidget {
  const _EnterYourMail();

  @override
  Widget build(BuildContext context) {
    return Text(
      AppLocalizations.of(context)!.please_enter_mail_adress_reset_password,
      style: Theme.of(context).textTheme.titleSmall,
    );
  }
}
