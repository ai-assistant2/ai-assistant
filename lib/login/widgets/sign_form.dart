import 'package:ai_assistant/auth/bloc/auth_bloc.dart';
import 'package:ai_assistant/login/widgets/sign_in_form.dart';
import 'package:ai_assistant/login/widgets/sign_up_form.dart';
import 'package:ai_assistant/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:formz/formz.dart';

import '../bloc/login_bloc.dart';

class SignForm extends StatelessWidget {
  const SignForm({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state.status == LoginStatus.fail) {
          bool isDialogOpen = _isThereCurrentDialogShowing(context);

          if (isDialogOpen) {
            Navigator.of(context).pop();
          }
        }
        if (state.formStatus.isInProgress) {
          bool isDialogOpen = _isThereCurrentDialogShowing(context);

          if (isDialogOpen) {
            Navigator.of(context).pop();
          }
          switch (state.authResult?.message ?? 'error') {
            case 'invalid-email':
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: Text(AppLocalizations.of(context)!.invalid_email),
                  ),
                );

              break;
            case 'user-not-found':
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: Text(AppLocalizations.of(context)!.user_not_found),
                  ),
                );
              break;
            case 'wrong-password':
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: Text(AppLocalizations.of(context)!.wrong_password),
                  ),
                );
              break;
            case 'email-already-in-use':
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: Text(
                        AppLocalizations.of(context)!.email_already_in_use),
                  ),
                );
              break;
            case 'weak-password':
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: Text(AppLocalizations.of(context)!.weak_password),
                  ),
                );
              break;
            default:
              debugPrint(state.authResult?.message);
              ScaffoldMessenger.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content:
                        Text(AppLocalizations.of(context)!.error_occured_auth),
                  ),
                );
          }
        }

        if (state.status == LoginStatus.success) {
          bool isDialogOpen = _isThereCurrentDialogShowing(context);

          if (isDialogOpen) {
            Navigator.of(context).pop();
          }
          context.read<AuthBloc>().add(SignIn(user: state.authResult!.user!));
        }
        if (state.status == LoginStatus.loading) {
          Utils.showLoadingIndicator(
              context, AppLocalizations.of(context)!.loading);
        }
      },
      builder: (context, state) {
        switch (state.status) {
          case LoginStatus.signUp:
            return const SignUpForm();

          case LoginStatus.signIn:
          default:
            return const SignInForm();
        }
      },
    );
  }

  showAlertDialog(BuildContext context, String title, String message) {
    return showCupertinoDialog(
      context: context,
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text(AppLocalizations.of(context)!.ok),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  showLoadingDialog(BuildContext context) {
    // set up the AlertDialog
    AlertDialog alert = const AlertDialog(
      content: SizedBox(
        height: 80,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
    // show the dialog
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async => false,
          child: alert,
        );
      },
    );
  }
}

_isThereCurrentDialogShowing(BuildContext context) =>
    ModalRoute.of(context)?.isCurrent != true;
