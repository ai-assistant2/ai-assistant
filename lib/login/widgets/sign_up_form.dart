import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:formz/formz.dart';

import '../bloc/login_bloc.dart';

class SignUpForm extends StatelessWidget {
  const SignUpForm({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: ListView(
        children: [
          Image.asset("assets/icon/iris_icon.png", scale: 2),
          const _GetOnBoardText(),
          // const _NameInput(),
          _EmailInput(),
          _PasswordInput(),
          _ContinueButton(),
          _AlreadyAccount(
              text: AppLocalizations.of(context)!.already_have_acccount)
        ],
      ),
    );
  }
}

class _GetOnBoardText extends StatelessWidget {
  const _GetOnBoardText();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(AppLocalizations.of(context)!.get_on_board,
            style: Theme.of(context).textTheme.headlineMedium),
      ],
    );
  }
}

class _AlreadyAccount extends StatelessWidget {
  final String text;
  const _AlreadyAccount({required this.text});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Padding(
        padding: const EdgeInsets.all(15),
        child: InkWell(
          onTap: () =>
              context.read<LoginBloc>().add(const SignChanged(method: 0)),
          child: Text(
            text,
            style: Theme.of(context)
                .textTheme
                .bodyLarge
                ?.copyWith(fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}

class _EmailInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      buildWhen: (previous, current) => previous.email != current.email,
      builder: (context, state) {
        return TextField(
          key: const Key('signUp_EmailInput_textField'),
          onChanged: (email) =>
              context.read<LoginBloc>().add(LoginEmailChanged(email)),
          decoration: InputDecoration(
            labelText: AppLocalizations.of(context)!.email,
            errorText: state.email.displayError != null
                ? AppLocalizations.of(context)!.invalid_email
                : null,
          ),
        );
      },
    );
  }
}

class _PasswordInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      buildWhen: (previous, current) => previous.password != current.password,
      builder: (context, state) {
        return TextField(
          key: const Key('signUp_passwordInput_textField'),
          onChanged: (password) =>
              context.read<LoginBloc>().add(LoginPasswordChanged(password)),
          obscureText: true,
          decoration: InputDecoration(
              labelText: AppLocalizations.of(context)!.password,
              errorText: state.password.displayError != null
                  ? validatePassword(state.password.value)
                  : null),
        );
      },
    );
  }

  String? validatePassword(String value) {
    if (!value.contains(RegExp(r'[a-z]'))) {
      return 'Password must contain at least one lowercase letter.';
    } else if (!value.contains(RegExp(r'[A-Z]'))) {
      return 'Password must contain at least one uppercase letter.';
    } else if (value.length < 8) {
      return 'Password muast at least contain 8 letters';
    } else if (!value.contains(RegExp(r'(?=.*?[0-9])'))) {
      return 'Password muast at least contain one number';
    }

    return null;
  }
}

class _ContinueButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      builder: (context, state) {
        return state.formStatus.isInProgress
            ? const Padding(
                padding: EdgeInsets.all(8.0),
                child: Center(child: CircularProgressIndicator()),
              )
            : InkWell(
                onTap: () => context.read<LoginBloc>().add(
                    const LoginSubmitted(signInMethod: SignInMethod.signUp)),
                child: Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Center(
                        child: Text(
                          AppLocalizations.of(context)!.continue_button,
                          style: Theme.of(context).textTheme.titleLarge,
                        ),
                      ),
                    ),
                  ),
                ),
              );
      },
    );
  }
}
