import 'dart:io';

import 'package:ai_assistant/login/views/forget_password_page.dart';
import 'package:ai_assistant/utils/utils.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:awesome_icons/awesome_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:formz/formz.dart';

import '../bloc/login_bloc.dart';

class SignInForm extends StatelessWidget {
  const SignInForm({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: ListView(
        padding: const EdgeInsets.only(bottom: 50, top: 50),
        children: [
          Image.asset(
            "assets/icon/iris_icon.png",
            height: 200,
          ),
          const _WelcomeText(),
          _EmailInput(),
          const Padding(padding: EdgeInsets.all(12)),
          _PasswordInput(),
          const Padding(padding: EdgeInsets.all(12)),
          _ForgetPassword(text: AppLocalizations.of(context)!.forget_password),
          _SignInButton(),
          _NoAccount(text: AppLocalizations.of(context)!.no_account),
          const _OrBar(),
          Platform.isIOS
              ? const _ContinueWithAppleButton()
              : const SizedBox.shrink(),
          const _SignInWithGoogleButton(),
          _ContinueAsAnonymous(),
        ],
      ),
    );
  }
}

class _WelcomeText extends StatelessWidget {
  const _WelcomeText();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(AppLocalizations.of(context)!.welcome_to,
            style: Theme.of(context).textTheme.headlineMedium),
        Text(AppLocalizations.of(context)!.iris,
            style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                fontWeight: FontWeight.bold,
                color: Theme.of(context).primaryColor))
      ],
    );
  }
}

class _NoAccount extends StatelessWidget {
  final String text;
  const _NoAccount({required this.text});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Padding(
        padding: const EdgeInsets.all(15),
        child: InkWell(
          onTap: () =>
              context.read<LoginBloc>().add(const SignChanged(method: 1)),
          child: Text(
            text,
            style: Theme.of(context)
                .textTheme
                .bodyLarge
                ?.copyWith(fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}

class _OrBar extends StatelessWidget {
  const _OrBar();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Expanded(
              child: Container(
            margin: const EdgeInsets.only(right: 10),
            child: Divider(
              color: Theme.of(context).textTheme.bodyMedium!.color,
            ),
          )),
          Text(AppLocalizations.of(context)!.or),
          Expanded(
              child: Container(
            margin: const EdgeInsets.only(left: 10),
            child: Divider(
              color: Theme.of(context).textTheme.bodyMedium!.color,
            ),
          )),
        ],
      ),
    );
  }
}

class _EmailInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      buildWhen: (previous, current) => previous.email != current.email,
      builder: (context, state) {
        return TextFormField(
          key: const Key('loginForm_usernameInput_textField'),
          onChanged: (email) =>
              context.read<LoginBloc>().add(LoginEmailChanged(email)),
          decoration: InputDecoration(
            labelText: AppLocalizations.of(context)!.email,
            errorText: state.email.displayError != null
                ? AppLocalizations.of(context)!.invalid_email
                : null,
          ),
        );
      },
    );
  }
}

class _PasswordInput extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      buildWhen: (previous, current) => previous.password != current.password,
      builder: (context, state) {
        return TextFormField(
          key: const Key('loginForm_passwordInput_textField'),
          onChanged: (password) =>
              context.read<LoginBloc>().add(LoginPasswordChanged(password)),
          obscureText: true,
          decoration: InputDecoration(
            labelText: AppLocalizations.of(context)!.password,
            errorText: state.password.displayError != null
                ? validatePassword(state.password.value)
                : null,
          ),
        );
      },
    );
  }

  String? validatePassword(String value) {
    if (!value.contains(RegExp(r'[a-z]'))) {
      return 'Password must contain at least one lowercase letter.';
    } else if (!value.contains(RegExp(r'[A-Z]'))) {
      return 'Password must contain at least one uppercase letter.';
    } else if (value.length < 8) {
      return 'Password muast at least contain 8 letters';
    } else if (!value.contains(RegExp(r'(?=.*?[0-9])'))) {
      return 'Password muast at least contain one number';
    }

    return null;
  }
}

class _SignInButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      builder: (context, state) {
        return state.formStatus.isInProgress
            ? const Padding(
                padding: EdgeInsets.all(8.0),
                child: Center(child: CircularProgressIndicator()),
              )
            : InkWell(
                onTap: () => context.read<LoginBloc>().add(
                    const LoginSubmitted(signInMethod: SignInMethod.email)),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Center(
                      child: Text(
                        AppLocalizations.of(context)!.sign_in,
                        style: Theme.of(context).textTheme.titleLarge,
                      ),
                    ),
                  ),
                ),
              );
      },
    );
  }
}

class _ContinueAsAnonymous extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      builder: (context, state) {
        return InkWell(
          onTap: () => context.read<LoginBloc>().add(
              const LoginSubmittedWith3rdParty(
                  signInMethod: SignInMethod.anonymous)),
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Center(
                child: Text(
                  AppLocalizations.of(context)!.continue_without_sign_in,
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class _ForgetPassword extends StatelessWidget {
  final String text;
  const _ForgetPassword({required this.text});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8),
      child: Align(
        alignment: Alignment.centerRight,
        child: InkWell(
            onTap: () =>
                Utils.navigatePage(context, const ForgetPasswordPage()),
            child: Text(
              text,
              style: Theme.of(context)
                  .textTheme
                  .bodyLarge
                  ?.copyWith(fontWeight: FontWeight.bold),
            )),
      ),
    );
  }
}

class _ContinueWithAppleButton extends StatelessWidget {
  const _ContinueWithAppleButton();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      builder: (context, state) {
        return InkWell(
          onTap: () => context.read<LoginBloc>().add(
              const LoginSubmittedWith3rdParty(
                  signInMethod: SignInMethod.apple)),
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(FontAwesomeIcons.apple),
                  const SizedBox(
                    width: 6,
                  ),
                  Text(
                    AppLocalizations.of(context)!.continue_with_apple,
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}

class _SignInWithGoogleButton extends StatelessWidget {
  const _SignInWithGoogleButton();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LoginBloc, LoginState>(
      builder: (context, state) {
        return InkWell(
          onTap: () => context.read<LoginBloc>().add(
              const LoginSubmittedWith3rdParty(
                  signInMethod: SignInMethod.google)),
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Center(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(FontAwesomeIcons.google),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    AppLocalizations.of(context)!.continue_with_google,
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ],
              )),
            ),
          ),
        );
      },
    );
  }
}

class SignInAnimatedText extends StatelessWidget {
  const SignInAnimatedText({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        const SizedBox(width: 20.0, height: 100.0),
        const Text(
          'Start to 🚀 your',
          style: TextStyle(fontSize: 43.0),
        ),
        const SizedBox(width: 20.0, height: 100.0),
        DefaultTextStyle(
          style: const TextStyle(
            fontSize: 40.0,
            fontFamily: 'Horizon',
          ),
          child: AnimatedTextKit(
            animatedTexts: [
              RotateAnimatedText('Career'),
              RotateAnimatedText('Knowledge'),
              RotateAnimatedText('personal life'),
            ],
            onTap: () {
              print("Tap Event");
            },
          ),
        ),
      ],
    );
  }
}
