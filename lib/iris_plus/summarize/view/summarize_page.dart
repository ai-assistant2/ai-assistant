import 'package:ai_assistant/iris_plus/iris_result/iris_plus_result.dart';
import 'package:ai_assistant/iris_plus/summarize/cubit/summarize_website_cubit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../ads/bloc/ads_bloc.dart';

class SummarizeWebsitePage extends StatefulWidget {
  const SummarizeWebsitePage({super.key, required this.title});
  final String title;

  @override
  State<SummarizeWebsitePage> createState() => _SummarizeWebsitePageState();
}

class _SummarizeWebsitePageState extends State<SummarizeWebsitePage> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocListener<SummarizeWebsiteCubit, SummarizeWebsiteState>(
      listener: (context, state) {
        if (state is SummarizeWebsiteLoaded) {
          showModalBottomSheet(
              context: context,
              builder: (context) => IrisPLusResult(
                    content: state.content,
                    title: widget.title,
                  ));

          context.read<AdsBloc>().add(AdsShowed());
        }
      },
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.9,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Scaffold(
            appBar: AppBar(
              elevation: 0,
              backgroundColor: Theme.of(context).scaffoldBackgroundColor,
              title: Text(
                AppLocalizations.of(context)!.summarize_website,
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(fontWeight: FontWeight.bold),
              ),
              centerTitle: false,
              automaticallyImplyLeading: false,
              actions: [
                Align(
                  alignment: Alignment.topRight,
                  child: RawMaterialButton(
                    onPressed: () => Navigator.of(context).pop(),
                    constraints: BoxConstraints.tight(const Size(36, 36)),
                    fillColor: Colors.black,
                    shape: const CircleBorder(),
                    child: const Icon(
                      CupertinoIcons.xmark,
                      size: 20.0,
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ),
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            body: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                    child: Text(
                      AppLocalizations.of(context)!.web_access_description,
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Theme.of(context).appBarTheme.backgroundColor,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10))),
                    child: CupertinoTextFormFieldRow(
                      onChanged: (value) => context
                          .read<SummarizeWebsiteCubit>()
                          .updateTextfield(value),
                      validator: (value) {
                        RegExp urlRegex = RegExp(
                          r'^(https?://)?'
                          r'((([a-zA-Z0-9-\._]+)\.([a-zA-Z]{2,6}))|'
                          r'(([0-9]{1,3}\.){3}[0-9]{1,3}))(\/.*)?$',
                        );
                        if (value == null || value.isEmpty) {
                          return AppLocalizations.of(context)!
                              .please_enter_valid_url;
                        } else if (!urlRegex.hasMatch(value)) {
                          return AppLocalizations.of(context)!.invalid_url;
                        }
                        return null;
                      },
                      enabled: true,
                      style: Theme.of(context).textTheme.bodyLarge,
                      placeholderStyle: Theme.of(context)
                          .textTheme
                          .bodyLarge
                          ?.copyWith(color: CupertinoColors.systemGrey),
                      placeholder: AppLocalizations.of(context)!.ask_something,
                      decoration: BoxDecoration(
                        color: Theme.of(context).appBarTheme.backgroundColor,
                      ),
                      prefix: const Icon(CupertinoIcons.link),
                      onSaved: (value) => context
                          .read<SummarizeWebsiteCubit>()
                          .updateTextfield(value ?? ''),
                    ),
                  ),
                  const SizedBox(height: 16.0),
                  BlocBuilder<SummarizeWebsiteCubit, SummarizeWebsiteState>(
                    builder: (context, state) {
                      return ElevatedButton(
                        style: ButtonStyle(
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.0),
                        ))),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            _formKey.currentState?.save();
                            context.read<SummarizeWebsiteCubit>().summarize(
                                AppLocalizations.of(context)!
                                    .summarize_this_website);
                          }
                        },
                        child: state == SummarizeWebsiteLoading()
                            ? const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: CircularProgressIndicator(),
                              )
                            : Text(
                                AppLocalizations.of(context)!.summarize,
                                style: Theme.of(context)
                                    .textTheme
                                    .titleLarge
                                    ?.copyWith(color: CupertinoColors.white),
                              ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
