import 'package:ai_assistant/repository/api_request.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'summarize_website_state.dart';

class SummarizeWebsiteCubit extends Cubit<SummarizeWebsiteState> {
  SummarizeWebsiteCubit() : super(SummarizeWebsiteInitial());

  String url = '';

  void summarize(String action) async {
    try {
      emit(SummarizeWebsiteLoading());

      final apiResponse = await HttpRequest().askChatWebAccess('$action $url');

      apiResponse != null
          ? emit(SummarizeWebsiteLoaded(apiResponse.mPT ?? 'null'))
          : emit(SummarizeWebsiteError());

      debugPrint(apiResponse?.mPT ?? '');
    } catch (e) {
      emit(SummarizeWebsiteError());
    }
  }

  void updateTextfield(String value) {
    url = value;
    emit(SummeraizeWebsiteUpdatedTextfield());
  }
}
