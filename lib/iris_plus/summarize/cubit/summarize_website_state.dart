part of 'summarize_website_cubit.dart';

class SummarizeWebsiteState extends Equatable {
  const SummarizeWebsiteState();

  @override
  List<Object> get props => [];
}

class SummarizeWebsiteInitial extends SummarizeWebsiteState {}

class SummeraizeWebsiteUpdatedTextfield extends SummarizeWebsiteState {}

class SummarizeWebsiteLoading extends SummarizeWebsiteState {}

class SummarizeWebsiteLoaded extends SummarizeWebsiteState {
  final String content;
  const SummarizeWebsiteLoaded(this.content);
}

class SummarizeWebsiteError extends SummarizeWebsiteState {}
