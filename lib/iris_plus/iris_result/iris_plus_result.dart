import 'package:ai_assistant/chat/widgets/chat_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class IrisPLusResult extends StatelessWidget {
  final String title;
  final String content;
  final String? question;
  const IrisPLusResult(
      {super.key, required this.content, required this.title, this.question});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        title: Text(
          title,
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
            onPressed: () => Clipboard.setData(ClipboardData(text: content)),
            icon: const Icon(
              CupertinoIcons.doc_on_clipboard,
            ),
            color: Theme.of(context).primaryColor,
          ),
          Align(
            alignment: Alignment.topRight,
            child: RawMaterialButton(
              onPressed: () => Navigator.of(context).pop(),
              constraints: BoxConstraints.tight(const Size(36, 36)),
              fillColor: Colors.black,
              shape: const CircleBorder(),
              child: const Icon(
                CupertinoIcons.xmark,
                size: 20.0,
                color: Colors.white,
              ),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                question == null
                    ? const SizedBox.shrink()
                    : Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Text(
                          question!.capitalize(),
                          textAlign: TextAlign.justify,
                          style: Theme.of(context)
                              .textTheme
                              .headlineSmall
                              ?.copyWith(fontWeight: FontWeight.bold),
                        ),
                      ),
                Text(
                  content.capitalize(),
                  textAlign: TextAlign.justify,
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  '👉 ${AppLocalizations.of(context)!.shorter_answer}'
                      .capitalize(),
                  textAlign: TextAlign.justify,
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
              ],
            )),
      ),
    );
  }
}
