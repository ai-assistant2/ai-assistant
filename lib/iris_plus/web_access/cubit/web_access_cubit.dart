import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import '../../../repository/api_request.dart';

part 'web_access_state.dart';

class WebAccessCubit extends Cubit<WebAccessState> {
  WebAccessCubit() : super(WebAccessInitial());

  String text = '';
  void search() async {
    try {
      emit(WebAccessLoading());

      final apiResponse = await HttpRequest().askChatWebAccess(text);

      apiResponse != null
          ? emit(WebAccessLoaded(text, apiResponse.mPT ?? ''))
          : emit(WebAccessError());

      debugPrint(apiResponse?.mPT ?? '');
    } catch (e) {
      emit(WebAccessError());
    }
  }

  void updateTextfield(String value) {
    text = value;
    emit(WebAccessUpdatedTextfield());
  }
}
