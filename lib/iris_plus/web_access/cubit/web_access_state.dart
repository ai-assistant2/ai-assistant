part of 'web_access_cubit.dart';

class WebAccessState extends Equatable {
  const WebAccessState();

  @override
  List<Object> get props => [];
}

class WebAccessInitial extends WebAccessState {}

class WebAccessUpdatedTextfield extends WebAccessState {}

class WebAccessLoaded extends WebAccessState {
  final String content;
  final String question;
  const WebAccessLoaded(this.question, this.content);
}

class WebAccessLoading extends WebAccessState {}

class WebAccessError extends WebAccessState {}
