import 'package:ai_assistant/iris_plus/web_access/cubit/web_access_cubit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../ads/bloc/ads_bloc.dart';
import '../../iris_result/iris_plus_result.dart';

class WebAccessPage extends StatefulWidget {
  const WebAccessPage({super.key});

  @override
  State<WebAccessPage> createState() => _WebAccessPageState();
}

class _WebAccessPageState extends State<WebAccessPage> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocListener<WebAccessCubit, WebAccessState>(
      listener: (context, state) {
        if (state is WebAccessLoaded) {
          showModalBottomSheet(
              isScrollControlled: true,
              context: context,
              builder: (context) => SizedBox(
                    height: MediaQuery.of(context).size.height * 0.9,
                    child: IrisPLusResult(
                      question: state.question,
                      content: state.content,
                      title: AppLocalizations.of(context)!.iris_with_web_access,
                    ),
                  ));

          context.read<AdsBloc>().add(AdsShowed());
        }
      },
      child: SizedBox(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Scaffold(
            appBar: AppBar(
              elevation: 0,
              backgroundColor: Theme.of(context).scaffoldBackgroundColor,
              title: Text(
                AppLocalizations.of(context)!.iris_with_web_access,
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(fontWeight: FontWeight.bold),
              ),
              centerTitle: false,
              automaticallyImplyLeading: false,
              actions: [
                Align(
                  alignment: Alignment.topRight,
                  child: RawMaterialButton(
                    onPressed: () => Navigator.of(context).pop(),
                    constraints: BoxConstraints.tight(const Size(36, 36)),
                    fillColor: Colors.black,
                    shape: const CircleBorder(),
                    child: const Icon(
                      CupertinoIcons.xmark,
                      size: 20.0,
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ),
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            body: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, bottom: 10),
                    child: Text(
                      AppLocalizations.of(context)!.web_access_description,
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Theme.of(context).appBarTheme.backgroundColor,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10))),
                    child: CupertinoTextFormFieldRow(
                      maxLines: 10,
                      onChanged: (value) =>
                          context.read<WebAccessCubit>().updateTextfield(value),
                      validator: (value) {
                        RegExp r = RegExp(r"^(?!([a-zA-Z])$).+$");
                        if (value == null || value.isEmpty) {
                          return AppLocalizations.of(context)!.academic_writing;
                        } else if (!r.hasMatch(value)) {
                          return AppLocalizations.of(context)!.invalid_url;
                        }
                        return null;
                      },
                      style: Theme.of(context).textTheme.bodyLarge,
                      placeholderStyle: Theme.of(context)
                          .textTheme
                          .bodyLarge
                          ?.copyWith(color: CupertinoColors.systemGrey),
                      placeholder: AppLocalizations.of(context)!.ask_something,
                      decoration: BoxDecoration(
                        color: Theme.of(context).appBarTheme.backgroundColor,
                      ),
                      onSaved: (value) => context
                          .read<WebAccessCubit>()
                          .updateTextfield(value ?? ''),
                    ),
                  ),
                  const SizedBox(height: 16.0),
                  BlocBuilder<WebAccessCubit, WebAccessState>(
                    builder: (context, state) {
                      return ElevatedButton(
                        style: ButtonStyle(
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.0),
                        ))),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            _formKey.currentState?.save();
                            context.read<WebAccessCubit>().search();
                          }
                        },
                        child: state == WebAccessLoading()
                            ? const Padding(
                                padding: EdgeInsets.all(8.0),
                                child: CircularProgressIndicator(),
                              )
                            : Text(
                                AppLocalizations.of(context)!.ask,
                                style: Theme.of(context)
                                    .textTheme
                                    .titleLarge
                                    ?.copyWith(color: CupertinoColors.white),
                              ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
