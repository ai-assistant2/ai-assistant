part of 'tts_bloc.dart';

abstract class TtsEvent extends Equatable {
  const TtsEvent();

  @override
  List<Object> get props => [];
}

class TtsInitialized extends TtsEvent {}

class TtsSpeaked extends TtsEvent {
  final String text;
  const TtsSpeaked({required this.text});
}
