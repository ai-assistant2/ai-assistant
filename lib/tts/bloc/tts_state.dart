part of 'tts_bloc.dart';

enum TtsStatus { playing, stopped, paused, continued }

class TtsState extends Equatable {
  const TtsState({this.ttsStatus = TtsStatus.stopped, this.text = ''});

  final TtsStatus ttsStatus;
  final String text;

  TtsState copyWith({TtsStatus? ttsStatus, String? text}) {
    return TtsState(
        ttsStatus: ttsStatus ?? this.ttsStatus, text: text ?? this.text);
  }

  @override
  List<Object> get props => [ttsStatus, text];
}
