import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_tts/flutter_tts.dart';

part 'tts_event.dart';
part 'tts_state.dart';

class TtsBloc extends Bloc<TtsEvent, TtsState> {
  late FlutterTts flutterTts;
  String? language;
  String? engine;
  double volume = 10.5;
  double pitch = 1.0;
  double rate = 0.5;
  bool isCurrentLanguageInstalled = false;

  String? _newVoiceText;
  int? _inputLength;

  TtsStatus ttsStatus = TtsStatus.paused;

  get isPlaying => TtsStatus == TtsStatus.playing;
  get isStopped => TtsStatus == TtsStatus.stopped;
  get isPaused => TtsStatus == TtsStatus.paused;
  get isContinued => TtsStatus == TtsStatus.continued;

  bool get isIOS => !kIsWeb && Platform.isIOS;
  bool get isAndroid => !kIsWeb && Platform.isAndroid;
  bool get isWindows => !kIsWeb && Platform.isWindows;
  bool get isWeb => kIsWeb;
  TtsBloc() : super(const TtsState()) {
    on<TtsInitialized>(_onTtsInitiated);
    on<TtsSpeaked>(_onTtsSpeaked);
  }

  Future _onTtsInitiated(TtsInitialized event, Emitter<TtsState> emit) async {
    flutterTts = FlutterTts();

    _setAwaitOptions();

    if (isAndroid) {
      _getDefaultEngine();
      _getDefaultVoice();
    }

    flutterTts.setStartHandler(() {
      emit(state.copyWith(ttsStatus: TtsStatus.playing));
    });

    if (isAndroid) {
      flutterTts.setInitHandler(() {});
    }

    flutterTts.setCompletionHandler(() {
      emit(state.copyWith(ttsStatus: TtsStatus.stopped));
    });

    flutterTts.setCancelHandler(() {
      emit(state.copyWith(ttsStatus: TtsStatus.stopped));
    });

    flutterTts.setPauseHandler(() {
      emit(state.copyWith(ttsStatus: TtsStatus.paused));
    });

    flutterTts.setContinueHandler(() {
      emit(state.copyWith(ttsStatus: TtsStatus.continued));
    });

    flutterTts.setErrorHandler((msg) {
      emit(state.copyWith(ttsStatus: TtsStatus.stopped));
    });
  }

  Future<dynamic> _getLanguages() async => await flutterTts.getLanguages;

  Future<dynamic> _getEngines() async => await flutterTts.getEngines;

  Future _getDefaultEngine() async {
    var engine = await flutterTts.getDefaultEngine;
    if (engine != null) {
      debugPrint(engine);
    }
  }

  Future _getDefaultVoice() async {
    var voice = await flutterTts.getDefaultVoice;
    if (voice != null) {
      debugPrint(voice);
    }
  }

  Future _onTtsSpeaked(TtsSpeaked event, Emitter<TtsState> emit) async {
    await flutterTts.setVolume(volume);
    await flutterTts.setSpeechRate(rate);
    await flutterTts.setPitch(pitch);

    if (event.text.isNotEmpty) {
      await flutterTts.speak(event.text);
    }
    }

  Future _setAwaitOptions() async {
    await flutterTts.awaitSpeakCompletion(true);
  }

  Future _stop() async {
    var result = await flutterTts.stop();
    if (result == 1) emit(state.copyWith(ttsStatus: TtsStatus.stopped));
  }

  Future _pause() async {
    var result = await flutterTts.pause();
    if (result == 1) emit(state.copyWith(ttsStatus: TtsStatus.paused));
  }

  void _onChange(String text) {
    emit(state.copyWith(text: text));
  }
}
