import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'bottom_bar_state.dart';

class BottomBarCubit extends Cubit<BottomBarState> {
  BottomBarCubit() : super(BottomBarState());

  void updateBottomBar(int index) {
    emit(BottomBarUpdated(index: index));
  }
}
