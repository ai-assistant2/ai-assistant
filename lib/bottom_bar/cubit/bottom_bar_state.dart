part of 'bottom_bar_cubit.dart';

class BottomBarState extends Equatable {
  const BottomBarState();

  @override
  List<Object> get props => [];
}

class BottomBarInitial extends BottomBarState {}

class BottomBarUpdated extends BottomBarState {
  final int index;

  const BottomBarUpdated({required this.index});

  @override
  List<Object> get props => [index];
}
