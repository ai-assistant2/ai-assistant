import 'package:ai_assistant/bottom_bar/cubit/bottom_bar_cubit.dart';
import 'package:ai_assistant/settings/view/settings_page.dart';
import 'package:ai_assistant/suggestion/views/suggestions_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../home/bloc/home_bloc.dart';
import '../../home/views/home_page.dart';
import '../../repository/auth_repository.dart';
import '../../repository/chat_repository.dart';
import '../../repository/firebase_repository.dart';
import '../../suggestion/cubit/suggestion_cubit.dart';

class BottomBarPage extends StatelessWidget {
  const BottomBarPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BottomBarCubit, BottomBarState>(
        builder: (context, state) {
      if (state is BottomBarUpdated) {
        return Scaffold(
            body: IndexedStack(
              index: state.index,
              children: [
                BlocProvider(
                  create: (context) => HomeBloc(
                      chatRepository: context.read<ChatRepository>(),
                      authRepository: context.read<AuthRepository>())
                    ..add(FetchedHomeData()),
                  child: const HomePage(),
                ),
                BlocProvider(
                  create: (context) => SuggestionCubit(
                      firebaseRepository: context.read<FirebaseRepository>())
                    ..loadSuggestions(),
                  child: const SuggestionsPage(),
                ),
                const SettingsPage(),
              ],
            ),
            bottomNavigationBar: SalomonBottomBar(
              currentIndex: state.index,
              onTap: (i) => context.read<BottomBarCubit>().updateBottomBar(i),
              items: [
                /// Home
                SalomonBottomBarItem(
                  icon: const Icon(Icons.chat),
                  title: Text(AppLocalizations.of(context)!.chats),
                  selectedColor: Colors.purple,
                ),

                /// Likes
                SalomonBottomBarItem(
                  icon: const Icon(Icons.short_text),
                  title: Text(AppLocalizations.of(context)!.suggestions),
                  selectedColor: Colors.pink,
                ),

                /// Profile
                SalomonBottomBarItem(
                  icon: const Icon(Icons.person),
                  title: Text(AppLocalizations.of(context)!.settings),
                  selectedColor: Colors.teal,
                ),
              ],
            ));
      } else {
        return const Scaffold(
          body: Center(child: CircularProgressIndicator()),
        );
      }
    });
  }
}
/*
 Scaffold(
        body: BlocProvider(
          create: (context) => HomeBloc(
              chatRepository: context.read<ChatRepository>(),
              authRepository: context.read<AuthRepository>())
            ..add(FetchedHomeData()),
          child: const HomePage(),
        ),
      )
    */