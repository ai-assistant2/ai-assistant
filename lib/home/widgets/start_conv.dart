import 'package:ai_assistant/chat/views/chat_page.dart';
import 'package:ai_assistant/repository/chat_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../chat/bloc/chat_bloc.dart';
import '../../utils/utils.dart';

class StartConv extends StatelessWidget {
  const StartConv({super.key});

  @override
  Widget build(BuildContext context) {
    return SliverPadding(
      padding: const EdgeInsets.only(top: 80, bottom: 20),
      sliver: SliverToBoxAdapter(
        child: _SearchCard(),
      ),
    );
  }
}

class _SearchCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: InkWell(
        onTap: () => Utils.navigatePage(
            context,
            BlocProvider(
              create: (context) => ChatBloc(
                  chatRepository: context.read<ChatRepository>(), convID: null)
                ..add(const UserDataLoaded()),
              child: const ChatPage(
                autofocus: true,
              ),
            )),
        child: Card(
          color: Colors.white,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          elevation: 4,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Icon(
                  CupertinoIcons.search,
                  color: Color.fromARGB(255, 89, 89, 90),
                ),
                const SizedBox(width: 8),
                Text(AppLocalizations.of(context)!.ask_something,
                    style: Theme.of(context).textTheme.titleMedium?.copyWith(
                          color: const Color.fromARGB(255, 89, 89, 90),
                        )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
