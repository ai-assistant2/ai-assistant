import 'package:ai_assistant/chat/widgets/chat_list.dart';
import 'package:ai_assistant/home/widgets/no_history.dart';
import 'package:ai_assistant/model/conv_metadata_firebase.dart';
import 'package:ai_assistant/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_swipe_action_cell/core/cell.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../chat/bloc/chat_bloc.dart';
import '../../chat/views/chat_page.dart';
import '../../repository/chat_repository.dart';
import '../bloc/home_bloc.dart';

class ConvList extends StatefulWidget {
  const ConvList({super.key});

  @override
  State<ConvList> createState() => _ConvListState();
}

class _ConvListState extends State<ConvList> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        if (state.status == HomeStatus.fail) {
          return const NoHistory();
        }
        if (state.status == HomeStatus.success) {
          return state.convMetadataList.isEmpty
              ? SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (context, index) {
                      return Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 15, right: 15),
                            child: _ConvItemEmptyList(
                              item: ConvMetadataFirebase(
                                  convID: 'convID',
                                  answer: AppLocalizations.of(context)!
                                      .how_can_assist_today,
                                  dtCreated:
                                      DateTime.now().millisecondsSinceEpoch,
                                  userID: 'userID',
                                  role: 'role',
                                  title: AppLocalizations.of(context)!
                                      .start_to_chat),
                              key: Key('$index'),
                            ),
                          ),
                          const Divider(
                            indent: 35,
                            color: Color.fromRGBO(43, 43, 43, 1),
                          )
                        ],
                      );
                    },
                    childCount: 1,
                  ),
                )
              : SliverPadding(
                  padding: const EdgeInsets.only(top: 20),
                  sliver: SliverList(
                    delegate: SliverChildBuilderDelegate((context, index) {
                      return index >= state.convMetadataList.length
                          ? const _BottomLoader()
                          : Padding(
                              padding: const EdgeInsets.only(
                                  //left: 15,
                                  ),
                              child: Column(
                                children: [
                                  SwipeActionCell(
                                      key: ObjectKey(index),
                                      trailingActions: <SwipeAction>[
                                        SwipeAction(
                                          title: AppLocalizations.of(context)!
                                              .delete,
                                          onTap: (CompletionHandler
                                              handler) async {
                                            context.read<HomeBloc>().add(
                                                ConvListItemRemoved(
                                                    index: index,
                                                    convID: state
                                                        .convMetadataList[index]
                                                        .convID));
                                          },
                                        )
                                      ],
                                      child: _ConvItem(
                                        item: state.convMetadataList[index],
                                        key: Key('$index'),
                                      )),
                                  const Divider(
                                    //indent: 20,
                                    color: CupertinoColors.systemGrey,
                                  )
                                ],
                              ),
                            );
                    },
                        childCount: state.hasReachedMax
                            ? state.convMetadataList.length
                            : state.convMetadataList.length + 1),
                  ),
                );
        } else {
          return const SliverToBoxAdapter(
            child: Center(child: CircularProgressIndicator()),
          );
        }
      },
    );
  }
}

class _ConvItemEmptyList extends StatefulWidget {
  final ConvMetadataFirebase? item;
  const _ConvItemEmptyList({required Key key, required this.item})
      : super(key: key);

  @override
  State<_ConvItemEmptyList> createState() => _ConvItemEmptyListState();
}

class _ConvItemEmptyListState extends State<_ConvItemEmptyList> {
  @override
  Widget build(BuildContext context) {
    return widget.item != null
        ? CupertinoListTile(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.4,
                  child: Text(
                    widget.item!.title.capitalize(),
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium
                        ?.copyWith(fontWeight: FontWeight.bold),
                  ),
                ),
                Text(
                  formatDate(DateTime.fromMillisecondsSinceEpoch(
                    widget.item!.dtCreated,
                  )),
                  style: Theme.of(context).textTheme.bodySmall,
                ),
              ],
            ),
            subtitle: Text(widget.item!.answer.capitalize(),
                style: Theme.of(context)
                    .textTheme
                    .bodyMedium
                    ?.copyWith(color: CupertinoColors.systemGrey)),
            onTap: () => Utils.navigatePage(
                context,
                BlocProvider(
                  create: (context) => ChatBloc(
                      chatRepository: context.read<ChatRepository>(),
                      convID: null)
                    ..add(const UserDataLoaded()),
                  child: const ChatPage(
                    autofocus: true,
                  ),
                )),
          )
        : const SizedBox.shrink();
  }

  String formatDate(DateTime date) {
    if (date.difference(DateTime.now()) > const Duration(days: 1)) {
      return DateFormat('MM/dd/yy').format(date);
    } else {
      final formatter = DateFormat('HH:mm').format(date);
      return formatter;
    }
  }
}

class _ConvItem extends StatefulWidget {
  final ConvMetadataFirebase? item;
  const _ConvItem({required Key key, required this.item}) : super(key: key);

  @override
  State<_ConvItem> createState() => _ConvItemState();
}

class _ConvItemState extends State<_ConvItem> {
  @override
  Widget build(BuildContext context) {
    return widget.item != null
        ? Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: CupertinoListTile(
              title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.4,
                    child: Text(
                      widget.item!.answer.capitalize(),
                      style: Theme.of(context).textTheme.titleLarge?.copyWith(),
                    ),
                  ),
                  Text(
                    formatDate(DateTime.fromMillisecondsSinceEpoch(
                      widget.item!.dtCreated,
                    )),
                    style: Theme.of(context).textTheme.bodySmall,
                  ),
                ],
              ),
              subtitle: Text(widget.item!.answer.capitalize(),
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium
                      ?.copyWith(color: CupertinoColors.systemGrey)),
              onTap: () => Utils.navigatePage(
                context,
                BlocProvider(
                  create: (context) => ChatBloc(
                      chatRepository: context.read<ChatRepository>(),
                      convID: widget.item!.convID)
                    ..add(const UserDataLoaded()),
                  child: const ChatPage(
                    autofocus: false,
                  ),
                ),
              ),
            ),
          )
        : const SizedBox.shrink();
  }

  String formatDate(DateTime date) {
    if (date.difference(DateTime.now()) > const Duration(days: 1)) {
      return DateFormat('MM/dd/yy').format(date);
    } else {
      final formatter = DateFormat('HH:mm').format(date);
      return formatter;
    }
  }
}

class _BottomLoader extends StatelessWidget {
  const _BottomLoader();

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.all(10.0),
      child: Center(
        child: SizedBox(
          height: 24,
          width: 24,
          child: CircularProgressIndicator(strokeWidth: 1.5),
        ),
      ),
    );
  }
}
