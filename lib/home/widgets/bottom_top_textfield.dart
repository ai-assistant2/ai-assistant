import 'package:ai_assistant/chat/bloc/chat_bloc.dart';
import 'package:ai_assistant/chat/views/chat_page.dart';
import 'package:ai_assistant/repository/chat_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class BottomToTopTextfield extends StatelessWidget {
  const BottomToTopTextfield({super.key});

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        decoration: BoxDecoration(
            color: Theme.of(context).appBarTheme.backgroundColor,
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(20), topRight: Radius.circular(20))),
        child: Container(
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          child: SizedBox(
            height: 100.0,
            width: MediaQuery.of(context).size.width,
            child: CupertinoTextField(
              // key: const Key('chat_textfield'),
              onTap: () => Navigator.of(context).push(_createRoute()),
              enabled: true,
              readOnly: true,

              autofocus: false,

              cursorColor: Theme.of(context).primaryColor,
              maxLines: 5,
              padding: const EdgeInsets.only(left: 20, bottom: 20, top: 10),
              style: Theme.of(context)
                  .textTheme
                  .titleMedium
                  ?.copyWith(fontSize: 20),
              textInputAction: TextInputAction.send,

              placeholder: AppLocalizations.of(context)!.ask_something,
              placeholderStyle: TextStyle(
                fontWeight: Theme.of(context).textTheme.bodySmall?.fontWeight,
                color: Theme.of(context)
                    .textTheme
                    .bodyMedium
                    ?.color
                    ?.withOpacity(0.7),
              ),
              suffix: Padding(
                  padding: const EdgeInsets.fromLTRB(9.0, 6.0, 9.0, 6.0),
                  child: Icon(CupertinoIcons.paperplane,
                      color: Theme.of(context).primaryColor)),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Theme.of(context).appBarTheme.backgroundColor,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Route _createRoute() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => BlocProvider(
        create: (context) => ChatBloc(
            convID: null, chatRepository: context.read<ChatRepository>())
          ..add(const UserDataLoaded()),
        child: const ChatPage(autofocus: false),
      ),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        const begin = Offset(0.0, 1.0);
        const end = Offset.zero;
        const curve = Curves.ease;

        var tween =
            Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }
}
