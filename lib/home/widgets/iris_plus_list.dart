import 'package:ai_assistant/iris_plus/summarize/cubit/summarize_website_cubit.dart';
import 'package:ai_assistant/iris_plus/summarize/view/summarize_page.dart';
import 'package:ai_assistant/iris_plus/web_access/cubit/web_access_cubit.dart';
import 'package:ai_assistant/iris_plus/web_access/view/web_access_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class IrisPlusList extends StatelessWidget {
  const IrisPlusList({super.key});

  @override
  Widget build(BuildContext context) {
    return const SliverPadding(
      padding: EdgeInsets.symmetric(vertical: 10),
      sliver: SliverToBoxAdapter(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          WebAccessElement(),
          SummarizeWebpageElement(),
        ],
      )),
    );
  }
}

class SummarizeWebpageElement extends StatelessWidget {
  const SummarizeWebpageElement({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 8, top: 8, bottom: 8),
      child: GestureDetector(
        onTap: () => showModalBottomSheet(
          isScrollControlled: true,
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          context: context,
          builder: (context) => BlocProvider<SummarizeWebsiteCubit>(
            create: (context) => SummarizeWebsiteCubit(),
            child: SummarizeWebsitePage(
              title: AppLocalizations.of(context)!.summarize,
            ),
          ),
        ),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: CupertinoColors.systemCyan),
          child: SizedBox(
            width: MediaQuery.of(context).size.width * 0.7,
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Icon(
                    CupertinoIcons.device_laptop,
                    size: 30,
                    color: Colors.white,
                  ),
                  Text(
                    AppLocalizations.of(context)!.summarize_website,
                    style: Theme.of(context)
                        .textTheme
                        .headlineSmall
                        ?.copyWith(color: Colors.white),
                  ),
                  Text(
                      AppLocalizations.of(context)!.ask_iris_summarize_websites,
                      style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                            color: Colors.white,
                          ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class WebAccessElement extends StatelessWidget {
  const WebAccessElement({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(left: 20, right: 8, top: 8, bottom: 8),
        child: GestureDetector(
          onTap: () => showCupertinoModalBottomSheet(
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            context: context,
            builder: (context) => BlocProvider(
              create: (context) => WebAccessCubit(),
              child: const WebAccessPage(),
            ),
          ),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Theme.of(context).primaryColor,
            ),
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.7,
                child: Column(
                  children: [
                    Align(
                        alignment: Alignment.topLeft,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Icon(
                              CupertinoIcons.globe,
                              size: 30,
                              color: Colors.white,
                            ),
                            Text(
                              AppLocalizations.of(context)!
                                  .iris_with_web_access,
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineSmall
                                  ?.copyWith(color: Colors.white),
                            ),
                            Text(
                                AppLocalizations.of(context)!
                                    .iris_with_web_access_description,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyLarge
                                    ?.copyWith(
                                      color: Colors.white,
                                    ))
                          ],
                        )),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
