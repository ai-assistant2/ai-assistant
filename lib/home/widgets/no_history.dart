import 'package:ai_assistant/chat/views/chat_page.dart';
import 'package:ai_assistant/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../chat/bloc/chat_bloc.dart';
import '../../repository/chat_repository.dart';

class NoHistory extends StatelessWidget {
  const NoHistory({super.key});

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
        key: const Key('no_history'),
        child: InkWell(
            onTap: () => Utils.navigatePage(
                context,
                BlocProvider(
                  create: (context) => ChatBloc(
                      chatRepository:
                          RepositoryProvider.of<ChatRepository>(context),
                      convID: null)
                    ..add(const UserDataLoaded()),
                  child: const ChatPage(
                    autofocus: true,
                  ),
                )),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Text(
                  AppLocalizations.of(context)!.no_item_for_now,
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ),
            )));
  }
}
