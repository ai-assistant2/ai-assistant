import 'package:ai_assistant/ressources/ressources.dart';
import 'package:awesome_icons/awesome_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../chat/bloc/chat_bloc.dart';
import '../../chat/views/chat_page.dart';
import '../../repository/chat_repository.dart';
import '../../utils/utils.dart';

class SuggestionList extends StatelessWidget {
  const SuggestionList({super.key});

  @override
  Widget build(BuildContext context) {
    List<String> suggestionList = [
      AppLocalizations.of(context)!.workout_routine,
      AppLocalizations.of(context)!.storytelling,
      AppLocalizations.of(context)!.problem_solving,
      AppLocalizations.of(context)!.academic_writing,
      AppLocalizations.of(context)!.translation,
      AppLocalizations.of(context)!.email,
      AppLocalizations.of(context)!.advices,
      AppLocalizations.of(context)!.explain_subjects,
      AppLocalizations.of(context)!.book_summary
    ];
    return SliverPadding(
        padding: const EdgeInsets.only(bottom: 150, left: 20, right: 20),
        sliver: SliverToBoxAdapter(
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 100,
            child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: suggestionList.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Chip(
                      label: Text(
                        suggestionList[index],
                        style: Theme.of(context).textTheme.bodyLarge,
                      ),
                    ),
                  );
                }),
          ),
        ));
  }
}

class SuggestionTile extends StatelessWidget {
  const SuggestionTile({super.key, required this.index});
  final int index;

  @override
  Widget build(BuildContext context) {
    List<String> suggestionList = [
      AppLocalizations.of(context)!.workout_routine,
      AppLocalizations.of(context)!.storytelling,
      AppLocalizations.of(context)!.problem_solving,
      AppLocalizations.of(context)!.academic_writing,
      AppLocalizations.of(context)!.translation,
      AppLocalizations.of(context)!.email,
      AppLocalizations.of(context)!.advices,
      AppLocalizations.of(context)!.explain_subjects,
      AppLocalizations.of(context)!.book_summary
    ];
    List<String> descriptionList = [
      AppLocalizations.of(context)!.workout_routine_description,
      AppLocalizations.of(context)!.storytelling_description,
      AppLocalizations.of(context)!.problem_solving_description,
      AppLocalizations.of(context)!.academic_writing_description,
      AppLocalizations.of(context)!.translation_description,
      AppLocalizations.of(context)!.email_description,
      AppLocalizations.of(context)!.advices_description,
      AppLocalizations.of(context)!.explain_description,
      AppLocalizations.of(context)!.book_summary_description,
    ];
    List<IconData> suggestionListIcon = [
      FontAwesomeIcons.baseballBall,
      CupertinoIcons.pen,
      CupertinoIcons.question,
      CupertinoIcons.lab_flask,
      CupertinoIcons.paperplane,
      FontAwesomeIcons.language,
      CupertinoIcons.rocket,
      FontAwesomeIcons.brain,
      CupertinoIcons.book,
    ];
    return GestureDetector(
      onTap: () => Utils.navigatePage(
          context,
          BlocProvider(
            create: (context) => ChatBloc(
                chatRepository: context.read<ChatRepository>(), convID: null)
              ..add(const UserDataLoaded())
              ..add(TextfieldUpdated(question: descriptionList[index])),
            child: ChatPage(
              autofocus: false,
            ),
          )),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30.0),
            color: Ressources().colors[index]),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Align(
                  alignment: Alignment.bottomLeft,
                  child: Icon(
                    suggestionListIcon[index],
                    size: 30,
                    color: Colors.white,
                  )),
              const SizedBox(height: 8),
              Text(
                suggestionList[index],
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 8),
              Text(
                descriptionList[index],
                overflow: TextOverflow.fade,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
