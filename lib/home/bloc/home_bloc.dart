import 'package:ai_assistant/model/conv_metadata_firebase.dart';
import 'package:ai_assistant/repository/auth_repository.dart';
import 'package:ai_assistant/repository/chat_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final ChatRepository _chatRepository;
  final AuthRepository _authRepository;
  HomeBloc(
      {required ChatRepository chatRepository,
      required AuthRepository authRepository})
      : _chatRepository = chatRepository,
        _authRepository = authRepository,
        super(const HomeState()) {
    on<FetchedHomeData>(_onFetchedHomeData);
    on<ConvListItemRemoved>(_onConvListItemRemoved);
    on<ConvListMoreItemsFetched>(_onConvListFetchedMoreItems);
  }

  Future _onConvListFetchedMoreItems(
      ConvListMoreItemsFetched event, Emitter<HomeState> emit) async {
    emit(state.copyWith(status: HomeStatus.loading));
    final user = await _authRepository.fetchUserUID();

    if (user != null) {
      await _chatRepository.initChatsStream(
          user, state.convMetadataList.last.dtCreated.toString());
    }
    emit(state.copyWith(
        convMetadataList: state.convMetadataList, status: HomeStatus.success));
  }

  Future _onConvListItemRemoved(
      ConvListItemRemoved event, Emitter<HomeState> emit) async {
    emit(state.copyWith(status: HomeStatus.loading));
    int index = event.index;
    String convID = event.convID;

    await _chatRepository.deleteChatMessage(convID);
    state.convMetadataList.removeAt(index);

    emit(state.copyWith(
        convMetadataList: state.convMetadataList, status: HomeStatus.success));
  }

  Future _onFetchedHomeData(
      FetchedHomeData event, Emitter<HomeState> emit) async {
    emit(state.copyWith(status: HomeStatus.loading));

    final user = await _authRepository.fetchUserUID();

    if (user != null) {
      await _chatRepository.initChatsStream(user, null);
    }

    await emit.forEach<List<ConvMetadataFirebase>>(
      _chatRepository.fetchConvMetadataList(),
      onData: (convMetadataList) {
        debugPrint('new data');
        convMetadataList.sort((a, b) => b.dtCreated.compareTo(a.dtCreated));

        return state.copyWith(
            status: HomeStatus.success,
            convMetadataList: convMetadataList,
            hasReachedMax: true);
      },
      onError: (_, __) => state.copyWith(
        status: HomeStatus.fail,
      ),
    );
  }
}
