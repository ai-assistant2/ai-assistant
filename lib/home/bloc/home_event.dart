part of 'home_bloc.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();

  @override
  List<Object> get props => [];
}

class FetchedHomeData extends HomeEvent {}

class ConvListItemRemoved extends HomeEvent {
  final int index;
  final String convID;
  const ConvListItemRemoved({required this.index, required this.convID});
}

class ConvListItemUpdated extends HomeEvent {}

class ConvListMoreItemsFetched extends HomeEvent {}
