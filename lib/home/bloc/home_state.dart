part of 'home_bloc.dart';

enum HomeStatus { initial, loading, success, fail }

class HomeState extends Equatable {
  const HomeState(
      {this.convMetadataList = const [],
      this.status = HomeStatus.initial,
      this.hasReachedMax = false});

  final List<ConvMetadataFirebase> convMetadataList;
  final HomeStatus status;
  final bool hasReachedMax;

  HomeState copyWith(
      {List<ConvMetadataFirebase>? convMetadataList,
      HomeStatus? status,
      bool? hasReachedMax}) {
    return HomeState(
        convMetadataList: convMetadataList ?? this.convMetadataList,
        status: status ?? this.status,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax);
  }

  @override
  List<Object> get props => [convMetadataList, status, hasReachedMax];
}
