import 'package:ai_assistant/chat/bloc/chat_bloc.dart';
import 'package:ai_assistant/chat/views/chat_page.dart';
import 'package:ai_assistant/home/widgets/home_appbar.dart';
import 'package:ai_assistant/repository/firebase_repository.dart';
import 'package:ai_assistant/utils/utils.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../repository/chat_repository.dart';
import '../widgets/conversation_list.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Stack(
        children: [
          Scaffold(
            key: const Key('homePage'),
            body: const CustomScrollView(
              slivers: <Widget>[
                HomeAppbar(),
                ConvList(),
              ],
            ),
            floatingActionButton: TextButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(
                    Theme.of(context).primaryColor),
              ),
              onPressed: () => Utils.navigatePage(
                  context,
                  BlocProvider(
                    create: (context) => ChatBloc(
                        chatRepository: context.read<ChatRepository>(),
                        convID: null)
                      ..add(const UserDataLoaded()),
                    child: const ChatPage(
                      autofocus: true,
                    ),
                  )),
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const Icon(
                      Icons.add,
                      color: Colors.white,
                    ),
                    Text(
                      AppLocalizations.of(context)!.new_chat,
                      style: Theme.of(context)
                          .textTheme
                          .bodyLarge
                          ?.copyWith(color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}



/*
*/