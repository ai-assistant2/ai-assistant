part of 'suggestion_cubit.dart';

class SuggestionState extends Equatable {
  const SuggestionState();

  @override
  List<Object> get props => [];
}

class SuggestionInitial extends SuggestionState {}

class SuggestionLoading extends SuggestionState {}

class SuggestionLoaded extends SuggestionState {
  final List<List<String>> suggestions;
  final List<String> titleList;
  const SuggestionLoaded({required this.suggestions, required this.titleList});
}
