import 'package:ai_assistant/model/suggestion_data.dart';
import 'package:ai_assistant/repository/firebase_repository.dart';
import 'package:ai_assistant/repository/temporary_local.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'suggestion_state.dart';

class SuggestionCubit extends Cubit<SuggestionState> {
  final FirebaseRepository firebaseRepository;
  SuggestionCubit({required this.firebaseRepository})
      : super(SuggestionInitial());

  void loadSuggestions() async {
    emit(SuggestionLoading());

    final result = SuggestionsData.fromJson(TemporaryLocal().suggestionsMap);

    final suggestions = result.suggestions;

    final List<String> titleList = [];

    for (var element in suggestions) {
      titleList.add(element.title);
    }

    final List<List<String>> promptList = [];

    for (var element in result.suggestions) {
      promptList.add(element.list);
    }

    emit(SuggestionLoaded(suggestions: promptList, titleList: titleList));
  }
}
