import 'package:ai_assistant/chat/bloc/chat_bloc.dart';
import 'package:ai_assistant/chat/views/chat_page.dart';
import 'package:ai_assistant/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'dart:math' as math;

import '../../repository/chat_repository.dart';
import '../cubit/suggestion_cubit.dart';

class SuggestionsPage extends StatelessWidget {
  const SuggestionsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[_AppBar(), _SuggestionList()],
      ),
    );
  }
}

class _AppBar extends StatelessWidget {
  const _AppBar({super.key});

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      backgroundColor: Colors.transparent,
      automaticallyImplyLeading: false,
      elevation: 0,
      title: Text(
        AppLocalizations.of(context)!.suggestions,
        style: Theme.of(context)
            .textTheme
            .headlineMedium
            ?.copyWith(fontWeight: FontWeight.bold),
      ),
      centerTitle: false,
    );
  }
}

class _SuggestionList extends StatelessWidget {
  const _SuggestionList({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SuggestionCubit, SuggestionState>(
      builder: (context, state) {
        if (state is SuggestionLoaded) {
          return SliverList.builder(
              itemCount: state.suggestions.length,
              itemBuilder: (context, index1) => Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 15, top: 10),
                        child: Text(
                          state.titleList[index1].capitalize(),
                          style: Theme.of(context)
                              .textTheme
                              .headlineSmall
                              ?.copyWith(fontWeight: FontWeight.bold),
                        ),
                      ),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.25,
                        width: MediaQuery.of(context).size.width,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: state.suggestions.length,
                          itemBuilder: (BuildContext context, int index2) {
                            return GestureDetector(
                              onTap: () => Utils.navigatePage(
                                  context,
                                  BlocProvider(
                                    //FIND AN EASIER WAY TO DO THIS
                                    create: (context) => ChatBloc(
                                        chatRepository:
                                            context.read<ChatRepository>(),
                                        convID: null)
                                      ..add(const UserDataLoaded())
                                      ..add(TextfieldUpdated(
                                          question: state.suggestions[index1]
                                              [index2])),
                                    child: ChatPage(
                                      autofocus: true,
                                    ),
                                  )),
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width * 0.5,
                                height:
                                    MediaQuery.of(context).size.height * 0.25,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      color: Color((math.Random().nextDouble() *
                                                  0xFFFFFF)
                                              .toInt())
                                          .withOpacity(0.5),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        state.suggestions[index1][index2],
                                        textAlign: TextAlign.center,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyLarge
                                            ?.copyWith(
                                                fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ));
        } else {
          return const SliverToBoxAdapter(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}

extension StringExtension on String {
  String capitalize() {
    if (isEmpty) {
      return this;
    }
    return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  }
}

List<String> categories = [
  "communication",
  "education",
  "essay",
  "finance",
  "health",
  "hobbies",
  "home",
  "job",
  "language",
  "math",
  "relationships",
  "resume",
  "social",
  "technology",
];
