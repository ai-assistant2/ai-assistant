import 'package:ai_assistant/chat/bloc/chat_bloc.dart';
import 'package:ai_assistant/repository/auth_repository.dart';
import 'package:ai_assistant/repository/chat_repository.dart';
import 'package:ai_assistant/repository/firebase_repository.dart';
import 'package:ai_assistant/repository/purchases_repository.dart';
import 'package:ai_assistant/theme/bloc/theme_bloc.dart';
import 'package:camera/camera.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

import 'firebase_options.dart';
import 'observer/simple_bloc_observer.dart';
import 'repository/camera_repository.dart';
import 'theme/view/theme_page.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  MobileAds.instance.initialize();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  Bloc.observer = SimpleBlocObserver();

  final cameras = await availableCameras();

  final firstCamera = cameras.first;

  runApp(RepositoryProvider(
    create: (context) => CameraRepository(firstCamera),
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    FirebaseRepository firebaseRepository = FirebaseRepository();
    final chatRepository = ChatRepository(
      firebaseRepository: firebaseRepository,
    );
    final authRepository = AuthRepository();
    final purchasesRepository = PurchasesRepository();

    return RepositoryProvider(
      create: (context) => firebaseRepository,
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => ThemeBloc()
              ..add(FetchTheme())
              ..add(LanguageStartChanged()),
          ),
          BlocProvider(
            create: (context) =>
                ChatBloc(convID: null, chatRepository: chatRepository),
          ),
        ],
        child: ThemePage(
          chatRepository: chatRepository,
          authRepository: authRepository,
          purchasesRepository: purchasesRepository,
        ),
      ),
    );
  }
}
