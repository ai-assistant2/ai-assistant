part of 'ads_bloc.dart';

enum AdsStatus { initial, show }

class AdsState extends Equatable {
  const AdsState(
      {this.interstitialAd, this.status = AdsStatus.initial, this.random = 0});

  final InterstitialAd? interstitialAd;
  final AdsStatus status;
  final int random;

  AdsState copyWith(
      {InterstitialAd? interstitialAd,
      AdsStatus? status,
      bool? showModal,
      int? random}) {
    return AdsState(
        interstitialAd: interstitialAd ?? this.interstitialAd,
        random: random ?? this.random,
        status: status ?? this.status);
  }

  @override
  List<Object?> get props => [status, interstitialAd, random];
}
