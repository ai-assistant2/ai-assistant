import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

part 'ads_event.dart';
part 'ads_state.dart';

class AdsBloc extends Bloc<AdsEvent, AdsState> {
  // int adCount = 0;
  InterstitialAd? _interstitialAd;
  final String _iosInterstitialID = 'ca-app-pub-9886479724480027/6659464072';
  final String _androidInsterstitialID =
      'ca-app-pub-9886479724480027/2897109198';
  AdsBloc() : super(const AdsState()) {
    on<AdsShowed>(_onAdsShowed);
    on<AdsLoaded>(_onAdsLoaded);
  }

  Future _onAdsLoaded(AdsLoaded event, Emitter<AdsState> emit) async {
    _loadRewardedAd();
  }

  Future _onAdsShowed(AdsShowed event, Emitter<AdsState> emit) async {
    emit(state.copyWith(status: AdsStatus.initial));
    if (_interstitialAd != null) {
      _interstitialAd?.fullScreenContentCallback = FullScreenContentCallback(
        onAdShowedFullScreenContent: (InterstitialAd ad) {},
        onAdFailedToShowFullScreenContent: (ad, error) {
          ad.dispose();
          _loadRewardedAd();
        },
        onAdDismissedFullScreenContent: (ad) {
          ad.dispose();
          _loadRewardedAd();
        },
      );

      _interstitialAd?.setImmersiveMode(true);
      // adCount++;
      final random = Random();
      // Randomly select between 0, 1, or 2 final
      final action = random.nextInt(3);

      emit(state.copyWith(
          random: action,
          status: AdsStatus.show,
          interstitialAd: _interstitialAd,
          showModal: true));
    }
  }

  void _loadRewardedAd() async {
    InterstitialAd.load(
        adUnitId: Platform.isIOS ? _iosInterstitialID : _androidInsterstitialID,
        request: const AdRequest(),
        adLoadCallback:
            InterstitialAdLoadCallback(onAdLoaded: (InterstitialAd ad) {
          _interstitialAd = ad;
        }, onAdFailedToLoad: (LoadAdError error) {
          _interstitialAd = null;
        }));
  }
}
