import 'dart:math';

import 'package:ai_assistant/bottom_bar/cubit/bottom_bar_cubit.dart';
import 'package:ai_assistant/bottom_bar/wiew/bottom_bar_page.dart';
import 'package:ai_assistant/repository/purchases_repository.dart';
import 'package:ai_assistant/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../home/bloc/home_bloc.dart';
import '../../home/views/home_page.dart';

import '../../repository/auth_repository.dart';
import '../../repository/chat_repository.dart';
import '../bloc/ads_bloc.dart';

class AdsPage extends StatelessWidget {
  const AdsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocListener<AdsBloc, AdsState>(
        listener: (context, state) async {
          if (state.status == AdsStatus.show) {
            bool isPremium = await context
                .read<PurchasesRepository>()
                .getSubscriptionStatus();

            if (isPremium) {
              debugPrint('isPremium');
            }

            if (!isPremium) {
              final random = Random();
              // Randomly select between 0, 1, or 2 final
              final action = random.nextInt(3);
              action != 0
                  ? Utils.showPurchasePage(context)
                  : state.interstitialAd?.show();
            }
          }
        },
        child: BlocProvider(
          create: (context) => BottomBarCubit()..updateBottomBar(0),
          child: const BottomBarPage(),
        ));
  }
}
