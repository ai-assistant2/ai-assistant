import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:purchases_flutter/purchases_flutter.dart';

class PurchasesRepository {
  CustomerInfo? customerInfo;
  Offerings? _offerings;
  bool isPremium = false;

  //final _subscriptionStreamController = BehaviorSubject<bool>.seeded(false);

  //Stream<bool> fetchSubscriptionStatus() =>
  //  _subscriptionStreamController.asBroadcastStream();

  Future<bool> isPro() async {
    return isPremium =
        customerInfo?.entitlements.all["premium_membership"]?.isActive ?? false;
  }

  Future<bool> getSubscriptionStatus() async {
    try {
      bool isConfigured = await Purchases.isConfigured;

      if (!isConfigured) {
        await initPlatformState();
      }
      customerInfo = await Purchases.getCustomerInfo();

      isPremium =
          customerInfo?.entitlements.all["premium_membership"]?.isActive ??
              false;

      // _subscriptionStreamController.add(isPremium ? true : false);

      return isPremium ? true : false;
    } on PlatformException catch (e) {
      debugPrint(e.toString());
      return false;
      //  return false;
    }
  }

  Future checkIfInitialized() async {
    bool isConfigured = await Purchases.isConfigured;

    return isConfigured;
  }

  Future initPlatformState() async {
    await Purchases.setDebugLogsEnabled(true);
    PurchasesConfiguration configuration;

    configuration = PurchasesConfiguration(Platform.isIOS
        ? "appl_wBXslWMoggmZBmRdvFMBHcfhYAl"
        : "goog_lSFprAoTwnBapgsLCyPbKzeWsdV");
    await Purchases.configure(configuration);
    print('*****************${configuration.appUserID}*********************');
  }

  Future<Offerings?> getOfferings() async {
    try {
      bool isConfigured = await Purchases.isConfigured;

      if (!isConfigured) {
        await initPlatformState();
      }

      _offerings = await Purchases.getOfferings();

      debugPrint('*****************${_offerings?.all}*********************');

      return _offerings;
    } on PlatformException catch (e) {
      print('${e.toString()}platform exception on configuration');
      return _offerings;
    }
  }

  Future<bool> restoreSubscription() async {
    try {
      bool isConfigured = await Purchases.isConfigured;

      if (!isConfigured) {
        await initPlatformState();
      }
      customerInfo = await Purchases.restorePurchases();

      isPremium = await getSubscriptionStatus();
      // _subscriptionStreamController.add(isPremium ? true : false);

      return isPremium ? true : false;
    } on PlatformException catch (e) {
      debugPrint(e.toString());
      return false;
    }
  }

  Future<bool> makePurchase(Package package) async {
    try {
      customerInfo = await Purchases.purchasePackage(package);

      isPremium = await getSubscriptionStatus();

      //  _subscriptionStreamController.add(isPremium ? true : false);

      debugPrint(isPremium.toString());
      return isPremium ? true : false;
    } on PlatformException catch (e) {
      var errorCode = PurchasesErrorHelper.getErrorCode(e);

      if (errorCode != PurchasesErrorCode.purchaseCancelledError) {}
      return false;
    }
  }
}
