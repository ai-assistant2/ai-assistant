import 'package:ai_assistant/model/chat_message.dart';
import 'package:ai_assistant/model/conv_metadata_firebase.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class FirebaseRepository {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  final _chatStreamController =
      BehaviorSubject<List<ConvMetadataFirebase>>.seeded(const []);

  Stream<List<ConvMetadataFirebase>> fetchConvMetadataList() =>
      _chatStreamController.asBroadcastStream();

  Future initStream(String userID, String? lastConvID) async {
    try {
      QuerySnapshot snapshot = lastConvID != null
          ? await _firestore
              .collection('conv')
              .where('userID', isEqualTo: userID)
              .orderBy('dt_created')
              .startAfter([lastConvID]).get()
          : await _firestore
              .collection('conv')
              .where('userID', isEqualTo: userID)
              .orderBy('dt_created')
              .limit(40)
              .get();

      List<ConvMetadataFirebase> convMetadataList = snapshot.docs.map((doc) {
        return ConvMetadataFirebase(
          convID: doc.get('convID') ?? '',
          role: doc.get('role') ?? '',
          dtCreated: doc.get('dt_created') ?? DateTime.now(),
          answer: doc.get('answer') == '' ? 'Error answer' : doc.get('answer'),
          title: doc.get('convTitle') ?? '',
          userID: doc.get('userID') ?? '',
        );
      }).toList();

      // If answer is empty, it produce an error

      convMetadataList.sort((a, b) => b.dtCreated.compareTo(a.dtCreated));

      _chatStreamController.add(convMetadataList);
    } catch (e) {
      _chatStreamController.add(const []);
      debugPrint(e.toString());
    }

    // _chatStreamController.add(const []);
  }

  Future fetchMoreItems(String userID) async {
    try {
      QuerySnapshot snapshot = await _firestore
          .collection('conv')
          .where('userID', isEqualTo: userID) //.startAfter(values)
          .get();
      List<ConvMetadataFirebase> convMetadatalist = snapshot.docs
          .map((doc) => ConvMetadataFirebase(
              convID: doc.get('convID'),
              role: doc.get('role'),
              dtCreated: doc.get('dt_created'),
              answer: doc.get('answer'),
              title: doc.get('convTitle'),
              userID: doc.get('userID')))
          .toList();
      convMetadatalist.sort((a, b) => a.dtCreated.compareTo(b.dtCreated));

      _chatStreamController.add(convMetadatalist);
    } catch (e) {
      _chatStreamController.add(const []);
      debugPrint(e.toString());
      rethrow;
    }
  }

  Future<String> createConv(
      ChatMessage chatMessage, String convTitle, String userID) async {
    try {
      final DocumentReference documentReference =
          _firestore.collection('conv').doc();

      final convMetadata = {
        'answer': chatMessage.answer,
        'dt_created': chatMessage.dtCreated,
        'convTitle': convTitle,
        'userID': userID,
        'role': chatMessage.role,
        'convID': documentReference.id
      };

      await documentReference.set(convMetadata);

      await documentReference.collection('messagesList').add({
        'dt_created': chatMessage.dtCreated,
        'role': chatMessage.role,
        'answer': chatMessage.answer,
        'hasError': chatMessage.hasError,
        'userID': chatMessage.userID
      });

      final chats = [..._chatStreamController.value];

      chats.indexWhere((t) => t.convID == documentReference.id);

      chats.add(ConvMetadataFirebase(
          answer: chatMessage.answer,
          dtCreated: chatMessage.dtCreated,
          role: chatMessage.role,
          title: convTitle,
          userID: userID,
          convID: documentReference.id));
      //  }

      _chatStreamController.add(chats);
      // print(chats.first.answer);

      return documentReference.id;
    } catch (e) {
      debugPrint("error adding chatMessage: $e");
      rethrow;
    }
  }

  Future<void> updateConv(
    ChatMessage chatMessage,
    String docPath,
  ) async {
    try {
      final DocumentReference documentReference =
          _firestore.collection('conv').doc(docPath);
      documentReference.update({
        'answer': chatMessage.answer,
        'dt_created': chatMessage.dtCreated,
        'userID': chatMessage.userID,
        'convID': documentReference.id,
        'role': chatMessage.role,
      });

      await documentReference.collection('messagesList').add({
        'dt_created': chatMessage.dtCreated,
        'answer': chatMessage.answer,
        'hasError': chatMessage.hasError,
        'userID': chatMessage.userID,
        'role': chatMessage.role,
      });

      final chats = [..._chatStreamController.value];
      final chatIndex =
          chats.indexWhere((t) => t.convID == documentReference.id);
      chats.removeAt(chatIndex);

      chats.add(ConvMetadataFirebase(
          answer: chatMessage.answer,
          dtCreated: chatMessage.dtCreated,
          role: chatMessage.role,
          title: docPath,
          userID: chatMessage.userID,
          convID: documentReference.id));

      _chatStreamController.add(chats);
    } catch (e) {
      debugPrint('error adding chat message: $e');
    }
  }

  Future<dynamic> deleteConv(String convID) async {
    try {
      final chat = [..._chatStreamController.value];
      final chatIndex = chat.indexWhere((t) => t.convID == convID);

      if (chatIndex == -1) {
        throw 'error';
      } else {
        await _firestore.collection('conv').doc(convID).delete();
        chat.removeAt(chatIndex);
        _chatStreamController.add(chat);
        return true;
      }
    } catch (e) {
      return false;
    }
  }

  Future<List<ChatMessage>> fetchConvMessagesList(
      String convID, int? dtCreated) async {
    try {
      //print(dtCreated);
      QuerySnapshot snapshot = dtCreated != null
          ? await _firestore
              .collection('conv')
              .doc(convID)
              .collection('messagesList')
              .orderBy(
                'dt_created',
              )
              .endBefore([dtCreated])
              .limitToLast(5)
              .get()
          : await _firestore
              .collection('conv')
              .doc(convID)
              .collection('messagesList')
              .orderBy(
                'dt_created',
              )
              .limitToLast(5)
              .get();

      List<ChatMessage> chatMessagelist = snapshot.docs
          .map((doc) => ChatMessage(
              role: doc.get('role'),
              dtCreated: doc.get('dt_created'),
              answer: doc.get('answer'),
              hasError: doc.get('hasError'),
              userID: doc.get('userID')))
          .toList();

      return chatMessagelist;
    } catch (e) {
      rethrow;
    }
  }
}
