

class SqfliteRepository {
  /*
  late Database _database;

  final _chatStreamController =
      BehaviorSubject<List<ConvMetadataFirebase>>.seeded(const []);

  Stream<List<ConvMetadataFirebase>> fetchConvMetadataList() =>
      _chatStreamController.asBroadcastStream();

  Future<int> insert(Map<String, dynamic> data) async {
    return await _database.insert('chat_database.db', data);
  }

  Future<List<Map<String, dynamic>>> initStream() async {
    return await _database.query('chat_database.db');
  }

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await opendatabase();
    return _database;
  }

  Future<Map<String, dynamic>> getById(int id) async {
    final List<Map<String, dynamic>> maps = await _database.query(
      'chat_database.db',
      where: 'id = ?',
      whereArgs: [id],
    );
    if (maps.isNotEmpty) {
      return maps.first;
    }
    return {};
  }

  Future<int> update(
    ChatMessage chatMessage,
  ) async {
    return await _database.update(
      'chat_database.db',
      data,
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  Future<List<Map<String, dynamic>>> getMessagesByConvID(String convID) async {
    final db = _database;
    return await db.query('messages', where: 'convID = ?', whereArgs: [convID]);
  }

  Future<void> insertConversation(Map<String, dynamic> convMetadata) async {
    final db = _database;
    await db.insert('conversations', convMetadata,
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<void> insertMessage(String convID, String message) async {
    final db = await database;
    final timestamp = DateTime.now().toIso8601String();
    await db.insert('messages',
        {'convID': convID, 'message': message, 'createdAt': timestamp},
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<List<ChatMessage>> getConversations() async {
    final db = await database;
    return await db.query('conversations').then((value) {});
  }

  Future<bool> deleteConv(String convID) async {
    try {
      final chat = [..._chatStreamController.value];
      final chatIndex = chat.indexWhere((t) => t.convID == convID);

      int result = await _database.delete(
        'chat_database.db',
        where: 'id = ?',
        whereArgs: [convID],
      );
      chat.removeAt(chatIndex);

      return result != null ? true : false;
    } catch (e) {
      debugPrint(e.toString());
      return false;
    }
  }

  Future<Database> opendatabase() async {
    final String databasesPath = await getDatabasesPath();
    const String dbName = 'chat_database.db';
    String path = join(databasesPath, dbName);

    return await openDatabase(
      path,
      version: 1,
      onCreate: (db, version) async {
        await db.execute('''
      CREATE TABLE conversations (
      id INTEGER PRIMARY KEY,
      convID TEXT,
      answer TEXT,
      dt_created TEXT,
      convTitle TEXT,
      userID TEXT,
      role TEXT
)
''');
        await db.execute('''
      CREATE TABLE messages (
      id INTEGER PRIMARY KEY,
      convID TEXT,
      message TEXT,
      createdAt TEXT
)
''');
      },
    );
  }

  */
}
