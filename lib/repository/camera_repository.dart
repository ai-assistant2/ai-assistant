import 'package:camera/camera.dart';

class CameraRepository {
  final CameraDescription camera;

  CameraRepository(this.camera);
}
