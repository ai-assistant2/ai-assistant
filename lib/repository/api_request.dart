import 'dart:convert';

import 'package:ai_assistant/model/gpt_response.dart';
import 'package:ai_assistant/model/gpt_web_access_response.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class HttpRequest {
  static Future askChat(String question) async {
    int retries = 0;

    while (retries < 3) {
      final url =
          Uri.parse('https://chatgpt-42.p.rapidapi.com/conversationgpt4');
      final headers = {
        'content-type': 'application/json',
        'X-RapidAPI-Key': 'b9819ecfdbmsh0f9229bb584aee5p1bf0dcjsn859d3b545032',
        'X-RapidAPI-Host': 'chatgpt-42.p.rapidapi.com',
      };
      final body = json.encode({
        'messages': [
          {
            'role': 'user',
            'content': question,
          },
        ],
        'system_prompt': '',
        'temperature': 0.5,
        'top_k': 50,
        'top_p': 0.9,
        'max_tokens': 1000,
      });

      try {
        final response = await http.post(url, headers: headers, body: body);
        if (response.statusCode == 200) {
          var result = GptResponse.fromJson(json.decode(
            utf8.decode(response.bodyBytes),
          ));

          return result;
        } else {
          print('Error: ${response.statusCode}');
        }
      } catch (error) {
        debugPrint(error.toString());
        retries++;
        await Future.delayed(const Duration(seconds: 1));
      }
    }

    return Future.error('failed to fetch data after 3 attempts');
  }

  Future<GptWebAccess?> askChatWebAccess(String question) async {
    final url = Uri.parse('https://open-ai21.p.rapidapi.com/conversationmpt');
    final headers = {
      'Content-Type': 'application/json',
      'X-Rapidapi-Key': 'b9819ecfdbmsh0f9229bb584aee5p1bf0dcjsn859d3b545032',
      'X-Rapidapi-Host': 'open-ai21.p.rapidapi.com',
    };
    final body = jsonEncode({
      'messages': [
        {'role': 'user', 'content': question}
      ],
      'web_access': true,
    });

    try {
      final response = await http.post(url, headers: headers, body: body);

      if (response.statusCode == 200) {
        var responseData =
            GptWebAccess.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));

        debugPrint(responseData.mPT);

        return responseData;
      } else {
        debugPrint('Request failed with status: ${response.statusCode}');
      }
    } catch (e) {
      debugPrint('Error: $e');
    }
    return null;
  }
}
