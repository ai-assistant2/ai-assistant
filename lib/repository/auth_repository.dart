import 'dart:async';
import 'dart:convert';

import 'package:ai_assistant/model/auth_result.dart';
import 'package:crypto/crypto.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class AuthRepository {
  final _firebaseAuth = FirebaseAuth.instance;

  final _userStreamController = BehaviorSubject<User?>.seeded(null);

  Stream<User?> fetchUser() => _userStreamController.asBroadcastStream();

  Stream<User?> currentUserStream() async* {
    final controller = StreamController<User?>();
    try {
      final user = _firebaseAuth.currentUser;
      _userStreamController.add(user);
    } catch (e) {
      controller.addError(e);
    }
    yield* controller.stream;
  }

  Future<void> initPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
  }

  Future<String?> fetchUserUID() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    try {
      prefs == null ? await initPreferences() : null;
      final String? userUID = prefs.getString('userID');

      if (userUID == null) {
        final user = _firebaseAuth.currentUser?.uid;
        return user;
      } else {
        return userUID;
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<AuthResult> signInAnonymously() async {
    try {
      UserCredential userCredential = await _firebaseAuth.signInAnonymously();
      _userStreamController.add(userCredential.user);
      debugPrint('Signed in anonymously as user ${userCredential.user?.uid}');

      return AuthResult(user: userCredential.user, message: "success");
    } catch (e) {
      _userStreamController.add(null);
      debugPrint('Error signing in anonymously: $e');

      return AuthResult(user: null, message: "unknown-error");
    }
  }

  Future<AuthResult> signUpWithEmail(
      {required String email, required String password}) async {
    try {
      UserCredential userCredential =
          await _firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      // Send email verification
      await userCredential.user?.sendEmailVerification();
      if (userCredential.user != null) {
        debugPrint('${userCredential.user}');
        return AuthResult(user: userCredential.user, message: 'success');
      } else {
        debugPrint('${userCredential.user}');
        return AuthResult(user: null, message: 'fail');
      }
    } on FirebaseAuthException catch (e) {
      _userStreamController.add(null);

      return AuthResult(user: null, message: e.code);
    }
  }

  Future<AuthResult> signInWithEmail(
      {required String email, required String password}) async {
    try {
      UserCredential userCredential = await _firebaseAuth
          .signInWithEmailAndPassword(email: email, password: password);
      _userStreamController.add(userCredential.user);

      if (userCredential.user != null) {
        return AuthResult(user: userCredential.user, message: 'success');
      } else {
        return AuthResult(user: null, message: 'fail');
      }
    } on FirebaseAuthException catch (e) {
      _userStreamController.add(null);

      return AuthResult(user: null, message: e.code);
    }
  }

  Future<void> signOut() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    try {
      _userStreamController.add(null);
      prefs.remove('userID');
      await _firebaseAuth.signOut();
    } catch (e) {
      throw Exception(e);
    }
  }

  Future<AuthResult> signInWithGoogle() async {
    try {
      final GoogleSignIn googleSignIn = GoogleSignIn();
// Start Google sign-in process
      final GoogleSignInAccount? googleSignInAccount =
          await googleSignIn.signIn();
      if (googleSignInAccount != null) {
        // Get Google authentication credentials
        final GoogleSignInAuthentication googleAuth = await googleSignInAccount
            .authentication; // Create Firebase credential with Google access token
        final AuthCredential credential = GoogleAuthProvider.credential(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        ); // Sign in with Firebase
        final UserCredential userCredential =
            await _firebaseAuth.signInWithCredential(credential);
        final User? user = userCredential.user;
        _userStreamController.add(userCredential.user);

        if (user != null) {
          _userStreamController.add(null);

          return AuthResult(user: user, message: 'success');
        } else {
          _userStreamController.add(null);

          return AuthResult(user: null, message: 'fail');
        }
      } else {
        return AuthResult(user: null, message: 'fail');
      }
    } on PlatformException {
      return AuthResult(user: null, message: 'fail');
    }
  }

  String sha256ofString(String input) {
    final bytes = utf8.encode(input);
    final digest = sha256.convert(bytes);
    return digest.toString();
  }

  Future<AuthResult> signInWithApple() async {
    final rawNonce = generateNonce();
    final nonce = sha256ofString(rawNonce);

    try {
      final appleCredential = await SignInWithApple.getAppleIDCredential(
          nonce: nonce,
          scopes: [
            AppleIDAuthorizationScopes.email,
            AppleIDAuthorizationScopes.fullName
          ]);

      final oauthCredential = OAuthProvider("apple.com").credential(
        idToken: appleCredential.identityToken,
        rawNonce: rawNonce,
      );

      final authResult =
          await _firebaseAuth.signInWithCredential(oauthCredential);

      final user = authResult.user;
      _userStreamController.add(user);

      if (user != null) {
        return AuthResult(user: user, message: 'success');
      } else {
        return AuthResult(user: null, message: 'fail');
      }
    } on FirebaseAuthException catch (e) {
      _userStreamController.add(null);

      return AuthResult(user: null, message: e.code);
    } on Exception {
      return AuthResult(user: null, message: 'fail');
    }
  }

  Future<String> resetPassword(String email) async {
    try {
      await _firebaseAuth.sendPasswordResetEmail(email: email);
      // Show success message to the user

      _userStreamController.add(null);

      return 'Password reset email sent';
    } on FirebaseAuthException catch (e) {
      return e.code;
    }
  }

  Future<void> deleteAccount() async {
    final user = _firebaseAuth.currentUser;
    try {
      await user?.delete();
    } catch (e) {}
  }
}
