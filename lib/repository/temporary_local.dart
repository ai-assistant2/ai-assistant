class TemporaryLocal {
  Map<String, dynamic> suggestionsMap = {
    "suggestions": [
      {
        "id": 1,
        "title": "hobbies",
        "list": [
          "Suggest new hobbies or activities I can try for relaxation and enjoyment.",
          "Recommend books, movies, or music based on my current hobbies and interests.",
          "What are some popular hobbies that people engage in for stress relief?",
          "Share ideas for DIY projects or crafts that I can do during my free time.",
          "Provide tips on how to turn a hobby into a side hustle or small business.",
          "Help me discover online communities related to my hobbies for networking.",
          "What are some unique and unusual hobbies that people might find interesting?",
          "Give suggestions for organizing and optimizing my hobby space at home.",
          "How can I find local clubs or meetups for specific hobbies in my area?",
          "Share interesting facts or trivia related to my favorite hobbies."
        ]
      },
      {
        "id": 2,
        "title": "home",
        "list": [
          "Suggest eco-friendly practices for maintaining a sustainable home environment.",
          "Provide home organization tips and tricks for maximizing space.",
          "What are the latest trends in home decor and interior design?",
          "Recommend smart home devices for energy efficiency and convenience.",
          "Share easy and quick recipes for homemade cleaning solutions.",
          "Give advice on creating a cozy and inviting atmosphere in my home.",
          "How can I incorporate mindfulness and relaxation practices into my home routine?",
          "Provide tips for budget-friendly home improvement projects.",
          "What are some innovative storage solutions for small living spaces?",
          "Help me plan a home workout routine without the need for gym equipment."
        ]
      },
      {
        "id": 3,
        "title": "job",
        "list": [
          "How can I improve my resume for a job application?",
          "Suggest effective strategies for job searching and networking.",
          "Provide tips for preparing and succeeding in job interviews.",
          "What are the key skills and qualifications employers look for in a candidate?",
          "Help me explore different career paths based on my interests and skills.",
          "Give advice on negotiating salary and benefits during a job offer.",
          "What are the latest trends in the job market for my industry?",
          "Assist me in creating a professional online presence for job opportunities.",
          "Provide guidance on balancing work and personal life for a healthier career.",
          "What are some ways to stay motivated and productive in a remote work environment?"
        ]
      },
      {
        "id": 4,
        "title": "language",
        "list": [
          "Recommend language learning apps or resources for beginners.",
          "Suggest effective techniques for practicing and improving language skills.",
          "Help me set realistic language learning goals and milestones.",
          "Provide tips for overcoming language learning challenges and plateaus.",
          "What are the benefits of learning multiple languages for personal and career growth?",
          "Share insights into language immersion experiences and their effectiveness.",
          "Assist me in finding language exchange partners or conversation groups.",
          "Recommend books, movies, or music in the language I'm learning.",
          "What are the cultural nuances and etiquette in different languages?",
          "Provide advice on preparing for language proficiency exams or certifications."
        ]
      },
      {
        "id": 5,
        "title": "math",
        "list": [
          "Suggest resources for learning and practicing fundamental math concepts.",
          "Provide tips for solving complex math problems and equations.",
          "Help me understand the practical applications of math in everyday life.",
          "What are some engaging and interactive ways to teach math to children?",
          "Recommend math-related books, documentaries, or podcasts for enthusiasts.",
          "Assist me in preparing for math competitions or exams.",
          "Share insights into careers that heavily involve mathematics.",
          "Provide guidance on using technology for mathematical modeling and simulations.",
          "What are the latest advancements in the field of mathematics?",
          "Help me overcome math anxiety and build confidence in my abilities."
        ]
      },
      {
        "id": 6,
        "title": "relationships",
        "list": [
          "Suggest effective communication strategies for building healthy relationships.",
          "Provide advice on navigating conflicts and disagreements in relationships.",
          "What are the key elements of a successful and lasting romantic relationship?",
          "Assist me in overcoming social anxiety and making new friends.",
          "Share tips for maintaining long-distance relationships.",
          "Help me understand the importance of boundaries in relationships.",
          "Provide guidance on building positive relationships in a professional setting.",
          "What are some signs of toxic relationships, and how can I address them?",
          "Share insights into the psychology of interpersonal relationships.",
          "Offer advice on strengthening relationships with family members."
        ]
      },
      {
        "id": 7,
        "title": "resume",
        "list": [
          "How can I create an impactful and visually appealing resume?",
          "Suggest key skills and achievements to include on my resume.",
          "Provide tips for tailoring my resume to specific job applications.",
          "Assist me in writing a compelling and personalized resume summary.",
          "What are the common mistakes to avoid in resume writing?",
          "Share examples of successful resumes in my industry.",
          "Provide insights into the use of keywords and ATS in resume optimization.",
          "Help me format my resume for a clean and professional look.",
          "What are the best practices for writing a cover letter that stands out?",
          "Offer advice on including volunteer work and extracurricular activities on a resume."
        ]
      },
      {
        "id": 8,
        "title": "social",
        "list": [
          "Suggest conversation starters for social events or networking.",
          "Provide tips on overcoming social anxiety in various situations.",
          "Help me navigate social dynamics in a new group or community.",
          "What are some strategies for building a strong social support network?",
          "Assist me in developing effective interpersonal communication skills.",
          "Share insights into the etiquette of social media interactions.",
          "Provide guidance on gracefully exiting or ending social engagements.",
          "What are the benefits of participating in social clubs or organizations?",
          "Help me understand cultural differences in social interactions.",
          "What are the signs of a healthy social life, and how can I achieve it?"
        ]
      },
      {
        "id": 9,
        "title": "technology",
        "list": [
          "Recommend resources for learning programming languages and coding.",
          "Suggest technology-related podcasts, blogs, or forums for staying informed.",
          "Provide insights into emerging technologies and their potential impact.",
          "Assist me in troubleshooting common tech issues and errors.",
          "What are the best practices for cybersecurity and online privacy?",
          "Share tips for effective time management in the tech industry.",
          "Help me understand the basics of data science and machine learning.",
          "Provide guidance on choosing the right tech stack for a project.",
          "What are the ethical considerations in the development and use of technology?",
          "Suggest strategies for continuous learning and professional development in technology."
        ]
      }
    ]
  };
}
