import '../model/chat_message.dart';
import '../model/conv_metadata_firebase.dart';
import 'firebase_repository.dart';

class ChatRepository {
  ChatRepository({
    required FirebaseRepository firebaseRepository,
  }) : _firebaseRepository = FirebaseRepository();

  final FirebaseRepository _firebaseRepository;

  Future<List<ChatMessage>> getConv(
    String convID,
    int? dtCreated,
  ) =>
      _firebaseRepository.fetchConvMessagesList(convID, dtCreated);

  Future initChatsStream(
    String userID,
    String? lastConvID,
  ) =>
      _firebaseRepository.initStream(userID, lastConvID);

  Stream<List<ConvMetadataFirebase>> fetchConvMetadataList() =>
      _firebaseRepository.fetchConvMetadataList();

  Future<void> saveChatMessage(
    ChatMessage chatMessage,
    String convTitle,
    String userID,
  ) =>
      _firebaseRepository.createConv(chatMessage, convTitle, userID);

  Future<String?> createConv(
    ChatMessage chatMessage,
    String convTitle,
    String userID,
  ) =>
      _firebaseRepository.createConv(chatMessage, convTitle, userID);

  Future<void> updateConv(
    ChatMessage chatMessage,
    String convTitle,
  ) =>
      _firebaseRepository.updateConv(chatMessage, convTitle);

  Future<void> deleteChatMessage(
    String convID,
  ) =>
      _firebaseRepository.deleteConv(convID);
}
