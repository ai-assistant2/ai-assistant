import 'package:ai_assistant/purchases/views/purchases_page.dart';
import 'package:ai_assistant/utils/webview_legal.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Utils {
  static Widget webviewPage(String url, String title, BuildContext context) {
    return WebviewLegal(url: url, title: title);
  }

  static Widget loadingWidget() {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  static Widget failureWidget(void action) {
    return Center(
      child: IconButton(
          onPressed: () => action, icon: const Icon(CupertinoIcons.refresh)),
    );
  }

  static Future showPurchasePage(BuildContext context) {
    return showModalBottomSheet(
      // useRootNavigator: true,
      isScrollControlled: true,
      context: context,
      builder: (context) => SizedBox(
          height: MediaQuery.of(context).copyWith().size.height * (0.90),
          child: const PurchasesPage()),
    );
  }

  static Future showPurchaseDialog(
      BuildContext context, String title, String content, bool purchased) {
    return showCupertinoDialog(
        useRootNavigator: true,
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
              title: Text(title),
              content: Text(content),
              actions: <Widget>[
                CupertinoDialogAction(
                  child: Text(AppLocalizations.of(context)!.ok),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ));
  }

  static void showLoadingIndicator(
    BuildContext context,
    String text,
  ) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return WillPopScope(
            onWillPop: () async => false,
            child: AlertDialog(
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0))),
                backgroundColor: Theme.of(context).scaffoldBackgroundColor,
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      text,
                      style: TextStyle(
                          color:
                              Theme.of(context).textTheme.displaySmall?.color),
                    ),
                    Container(
                        padding: const EdgeInsets.only(top: 10),
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                            Theme.of(context).primaryColor,
                          ),
                        ))
                  ],
                )));
      },
    );
  }

  static Future showDisclaimer(BuildContext context) async {
    return showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: const Text('Oups!'),
            content: const Text(
              'This message was not forwarded. Do you want to send it back?',
              textAlign: TextAlign.justify,
            ),
            actions: <Widget>[
              CupertinoDialogAction(
                  onPressed: () => Navigator.of(context).pop(),
                  child: const Text('Resend')),
              CupertinoDialogAction(
                  onPressed: () => Navigator.of(context).pop(),
                  child: const Text('Cancel')),
            ],
          );
        });
  }

  static Future navigatePage(BuildContext context, Widget widget) {
    return Navigator.of(context, rootNavigator: true)
        .push(MaterialPageRoute(builder: (context) => widget));
  }

  static void showLoadingDialog(BuildContext context) {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return Container(
            padding: const EdgeInsets.all(16),
            color: Colors.black.withOpacity(0.8),
            child: const Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: EdgeInsets.only(bottom: 16),
                    child: SizedBox(
                        width: 32,
                        height: 32,
                        child: CircularProgressIndicator(strokeWidth: 3)),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 4),
                    child: Text(
                      'Please wait …',
                      style: TextStyle(color: Colors.white, fontSize: 16),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Text(
                    'Loading',
                    style: TextStyle(color: Colors.white, fontSize: 14),
                    textAlign: TextAlign.center,
                  )
                ]));
      },
    );
  }
}
