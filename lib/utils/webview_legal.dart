import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebviewLegal extends StatefulWidget {
  final String url;
  final String title;
  const WebviewLegal({super.key, required this.url, required this.title});

  @override
  State<WebviewLegal> createState() => _WebviewLegalState();
}

class _WebviewLegalState extends State<WebviewLegal> {
  final Set<Factory<OneSequenceGestureRecognizer>> gestureRecognizers = {
    Factory(() => EagerGestureRecognizer())
  };
  WebViewController controller = WebViewController();
  bool _isLoading = true;
  bool _isMounted = false;
  @override
  void initState() {
    super.initState();
    _isMounted = true;

    controller = WebViewController()
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(NavigationDelegate(
        onProgress: (int progress) {},
        onPageStarted: (start) {},
        onPageFinished: (url) => _loadPage(),
      ))
      ..loadRequest(Uri.parse(widget.url));
  }

  @override
  void dispose() {
    _isMounted = false;
    super.dispose();
  }

  void _updateState() {
    if (_isMounted) {
      setState(() {});
    }
  }

  _loadPage() async {
    if (mounted) {
      // await Future.delayed(const Duration(seconds: 1));
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Privacy Policy')),
      body: Center(
          child: _isLoading
              ? const CircularProgressIndicator()
              : WebViewWidget(
                  controller: controller,
                  gestureRecognizers: gestureRecognizers,
                )),
    );
  }
}
