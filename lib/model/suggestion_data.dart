class SuggestionsData {
  List<CategoryModel> suggestions;

  SuggestionsData({required this.suggestions});

  factory SuggestionsData.fromJson(Map<String, dynamic> json) {
    List<CategoryModel> suggestions = List<CategoryModel>.from(
      json['suggestions']
          .map((categoryJson) => CategoryModel.fromJson(categoryJson)),
    );

    return SuggestionsData(suggestions: suggestions);
  }
}

class CategoryModel {
  int id;
  String title;
  List<String> list;

  CategoryModel({required this.id, required this.title, required this.list});

  factory CategoryModel.fromJson(Map<String, dynamic> json) {
    return CategoryModel(
      id: json['id'],
      title: json['title'],
      list: List<String>.from(json['list']),
    );
  }
}
