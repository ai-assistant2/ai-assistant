class FilteredIndex {
  int? index;
  String word;

  FilteredIndex({required this.word, required this.index});
}
