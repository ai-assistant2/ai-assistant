class GptWebAccess {
  String? mPT;
  int? servercode;
  bool? webAccess;

  GptWebAccess({this.mPT, this.servercode, this.webAccess});

  GptWebAccess.fromJson(Map<String, dynamic> json) {
    mPT = json['MPT'];
    servercode = json['servercode'];
    webAccess = json['web_access'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['MPT'] = mPT;
    data['servercode'] = servercode;
    data['web_access'] = webAccess;
    return data;
  }
}
