class ChatMessage {
  String role;
  int dtCreated;
  String userID;
  String answer;
  bool hasError;

  ChatMessage({
    required this.hasError,
    required this.answer,
    required this.dtCreated,
    required this.role,
    required this.userID,
  });
}
