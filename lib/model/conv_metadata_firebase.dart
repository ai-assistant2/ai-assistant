class ConvMetadataFirebase {
  String convID;
  int dtCreated;
  String answer;
  String title;
  String userID;
  String role;

  ConvMetadataFirebase(
      {required this.answer,
      required this.role,
      required this.dtCreated,
      required this.convID,
      required this.title,
      required this.userID});
}
