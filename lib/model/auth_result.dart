import 'package:firebase_auth/firebase_auth.dart';

class AuthResult {
  AuthResult({required this.user, required this.message});

  final User? user;
  final String message;
}
