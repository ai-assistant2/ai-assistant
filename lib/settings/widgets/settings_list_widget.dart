import 'dart:io';

import 'package:ai_assistant/auth/bloc/auth_bloc.dart';
import 'package:ai_assistant/repository/purchases_repository.dart';
import 'package:ai_assistant/utils/utils.dart';
import 'package:ai_assistant/utils/webview_legal.dart';
import 'package:awesome_icons/awesome_icons.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:in_app_review/in_app_review.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart' as mbs;
import 'package:settings_ui/settings_ui.dart';

import '../../theme/bloc/theme_bloc.dart';
import '../../theme/theme_data/app_theme.dart';

class SettingsListWidget extends StatefulWidget {
  const SettingsListWidget({super.key});

  @override
  State<SettingsListWidget> createState() => _SettingsListWidgetState();
}

class _SettingsListWidgetState extends State<SettingsListWidget> {
  final InAppReview inAppReview = InAppReview.instance;

  @override
  Widget build(BuildContext context) {
    bool isDarkMode = context.read<ThemeBloc>().state.themeData ==
        appThemeData[AppTheme.blueDark];

    return SettingsList(
        darkTheme: SettingsThemeData(
          settingsListBackground: Theme.of(context).scaffoldBackgroundColor,
        ),
        sections: [
          SettingsSection(
            title: Text(AppLocalizations.of(context)!.general),
            tiles: <SettingsTile>[
              SettingsTile.navigation(
                onPressed: (context) => _showActionSheet(context),
                leading: Icon(CupertinoIcons.flag,
                    color: Theme.of(context).primaryColor),
                title: Text(AppLocalizations.of(context)!.language),
                value: Text(AppLocalizations.of(context)!.current_language),
              ),
              SettingsTile.switchTile(
                onToggle: (toogleIsDarkMode) {
                  toogleIsDarkMode
                      ? context
                          .read<ThemeBloc>()
                          .add(const ThemeChanged(theme: AppTheme.blueDark))
                      : context
                          .read<ThemeBloc>()
                          .add(const ThemeChanged(theme: AppTheme.blueLight));
                  setState(() {});
                },
                initialValue: isDarkMode ? true : false,
                leading: Icon(CupertinoIcons.moon,
                    color: Theme.of(context).primaryColor),
                title:
                    Text(AppLocalizations.of(context)?.dark_theme ?? 'General'),
              ),
            ],
          ),
          SettingsSection(
            title: Text(AppLocalizations.of(context)!.more),
            tiles: <SettingsTile>[
              SettingsTile(
                  leading: const Icon(
                    FontAwesomeIcons.crown,
                    color: CupertinoColors.systemYellow,
                  ),
                  title: Text(AppLocalizations.of(context)!.restore_purchases),
                  onPressed: (pressed) async {
                    Utils.showLoadingIndicator(
                        context, AppLocalizations.of(context)!.loading);

                    await context
                        .read<PurchasesRepository>()
                        .restoreSubscription()
                        .then((isPremium) {
                      if (isPremium) {
                        Navigator.of(context).pop();
                        Utils.showPurchaseDialog(
                            context,
                            AppLocalizations.of(context)!
                                .purchases_restored_success,
                            AppLocalizations.of(context)!.purchases_restored,
                            true);
                      } else {
                        Navigator.of(context).pop();
                        Utils.showPurchaseDialog(
                            context,
                            'Purchases not restored',
                            'Purchases has not been restored.',
                            true);
                      }
                    });
                  }),
            ],
          ),
          SettingsSection(
            title: Text(AppLocalizations.of(context)!.legal),
            tiles: <SettingsTile>[
              SettingsTile.navigation(
                leading: Icon(CupertinoIcons.doc,
                    color: Theme.of(context).primaryColor),
                title: Text(
                  AppLocalizations.of(context)!.terms_conditions,
                ),
                onPressed: (pressed) => mbs.showCupertinoModalBottomSheet(
                    context: context,
                    builder: (context) => AnimatedContainer(
                          duration: const Duration(seconds: 1),
                          child: WebviewLegal(
                            url: 'https://kairos-apps.com/privacy-policy/',
                            title:
                                AppLocalizations.of(context)!.terms_conditions,
                          ),
                        )),
              ),
              SettingsTile.navigation(
                leading: Icon(CupertinoIcons.doc_person,
                    color: Theme.of(context).primaryColor),
                title: Text(AppLocalizations.of(context)!.privacy_policy),
                onPressed: (pressed) => mbs.showCupertinoModalBottomSheet(
                    context: context,
                    //  enableDrag: true,
                    builder: (context) => AnimatedContainer(
                          duration: const Duration(milliseconds: 5000),
                          child: WebviewLegal(
                            url:
                                'https://kairos-apps.com/terms-and-conditions/',
                            title: AppLocalizations.of(context)!.privacy_policy,
                          ),
                        )),
              ),
            ],
          ),
          SettingsSection(
            title: Text(AppLocalizations.of(context)!.more),
            tiles: <SettingsTile>[
              SettingsTile.navigation(
                leading: const Icon(
                  CupertinoIcons.star,
                  color: CupertinoColors.systemYellow,
                ),
                title: Text(AppLocalizations.of(context)!.rate_us),
                onPressed: (pressed) => inAppReview.openStoreListing(
                    appStoreId: Platform.isIOS
                        ? '1668871275'
                        : 'com.kairos.ai_assistant'),
              ),
            ],
          ),
          SettingsSection(
            title: Text(AppLocalizations.of(context)!.account),
            tiles: <SettingsTile>[
              SettingsTile.navigation(
                  title: Text(AppLocalizations.of(context)!.delete_account),
                  leading: const Icon(CupertinoIcons.delete,
                      color: CupertinoColors.destructiveRed),
                  onPressed: (pressed) =>
                      _showActionSheetDeleteAccount(context)),
              SettingsTile(
                  title: Center(
                    child: Text(
                      AppLocalizations.of(context)!.sign_out,
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium
                          ?.copyWith(color: CupertinoColors.destructiveRed),
                    ),
                  ),
                  onPressed: (pressed) {
                    // Navigator.of(context).pop();
                    context.read<AuthBloc>().add(SignOut());
                  }),
            ],
          ),
        ]);
  }

  void _showActionSheet(BuildContext context) {
    showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
          cancelButton: CupertinoActionSheetAction(
            child: Text(
              AppLocalizations.of(context)!.cancel,
              style: const TextStyle(color: CupertinoColors.systemRed),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(AppLocalizations.of(context)!.language),
          message: Text(AppLocalizations.of(context)!.select_your_language),
          actions: context
              .findAncestorWidgetOfExactType<MaterialApp>()
              ?.supportedLocales
              .map((locale) => CupertinoActionSheetAction(
                    onPressed: () {
                      context.read<ThemeBloc>().add(LanguageChanged(locale));
                      Navigator.of(context).pop();
                    },
                    child: Text(locale.fullName()),
                  ))
              .toList()),
    );
  }

  // This shows a CupertinoModalPopup which hosts a CupertinoActionSheet.
  void _showActionSheetDeleteAccount(BuildContext context) {
    showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
        title: const Text('Delete'),
        message: const Text('Are you sure to delete?'),
        actions: <CupertinoActionSheetAction>[
          CupertinoActionSheetAction(
            isDestructiveAction: true,
            onPressed: () {
              context.read<AuthBloc>().add(AccountDeleted());

              Navigator.pop(context);
            },
            child: Text(AppLocalizations.of(context)!.delete),
          ),
          CupertinoActionSheetAction(
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text(AppLocalizations.of(context)!.cancel),
          ),
        ],
      ),
    );
  }
}

extension FullName on Locale {
  String fullName() {
    switch (languageCode) {
      case 'en':
        return 'English';
      case 'ar':
        return 'عرب';
      case 'fr':
        return 'Français';
      case 'es':
        return 'Español';
      case 'de':
        return 'Deutsch';
      case 'ko':
        return '한국인';
      case 'ja':
        return '日本';
      case 'pt':
        return 'Português';
      case 'it':
        return 'Italiano';
      case 'zh':
        return '中国人';
    }
    return 'English';
  }
}
