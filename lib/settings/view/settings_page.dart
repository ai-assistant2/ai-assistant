import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart' as mbs;
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../widgets/settings_list_widget.dart';

class SettingsPage extends StatelessWidget {
  const SettingsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        title: Text(
          AppLocalizations.of(context)!.settings,
          style: Theme.of(context)
              .textTheme
              .headlineMedium
              ?.copyWith(fontWeight: FontWeight.bold),
        ),
        centerTitle: false,
        automaticallyImplyLeading: false,
      ),
      body: mbs.CupertinoScaffold(
        body: Builder(
            builder: (context) => CupertinoPageScaffold(
                    child: mbs.CupertinoScaffold(
                        body: Material(
                  color: Theme.of(context).scaffoldBackgroundColor,
                  child: const SizedBox.expand(
                    child: CustomScrollView(
                      physics: NeverScrollableScrollPhysics(),
                      slivers: [
                        SliverFillRemaining(
                          child: SettingsListWidget(),
                        ),
                      ],
                    ),
                  ),
                )))),
      ),
    );
  }
}
