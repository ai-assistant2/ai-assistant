import 'dart:io';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:ai_assistant/ocr/cubit/ocr_cubit.dart';
import 'package:ai_assistant/repository/camera_repository.dart';
import 'package:app_settings/app_settings.dart';
import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_mlkit_text_recognition/google_mlkit_text_recognition.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../chat/bloc/chat_bloc.dart';

class OcrButton extends StatelessWidget {
  const OcrButton({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 40,
      child: TextButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
          padding: const EdgeInsets.all(20),
          disabledBackgroundColor: Colors.transparent,
        ),
        onPressed: () async {
          var cameraStatus = await Permission.camera.status;

          var microphoneStatus = await Permission.microphone.status;

          if (cameraStatus.isGranted && microphoneStatus.isGranted) {
            //TODO AVOID ASYNC CALLS IN BUILD METHOD
            showModalBottomSheet(
                isScrollControlled: true,
                context: context,
                builder: (context) => SizedBox(
                    height: MediaQuery.of(context).size.height * 0.9,
                    child: BlocProvider(
                      create: (context) => OcrCubit()..onTakeApictureLoaded(),
                      child: const OcrPage(),
                    ))).then((value) => {
                  value != null
                      ? context
                          .read<ChatBloc>()
                          .add(TextfieldUpdated(question: value.toString()))
                      : null
                });
          } else {
            var statuses = await [
              Permission.camera,
              Permission.microphone,
            ].request();

            var updatedCameraStatus = statuses[Permission.camera]!;
            var updatedMicrophoneStatus = statuses[Permission.microphone]!;

            if (updatedCameraStatus != PermissionStatus.granted) {
              AppSettings.openAppSettings();
              return;
            }

            if (updatedMicrophoneStatus != PermissionStatus.granted) {
              AppSettings.openAppSettings();
              return;
            }
          }
        },
        child: Icon(Icons.camera_alt_rounded,
            size: 30, color: Theme.of(context).primaryColor),
      ),
    );
  }
}

class OcrPage extends StatelessWidget {
  const OcrPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OcrCubit, OcrState>(
      builder: (context, state) {
        if (state is ValidateOcrLoaded) {
          return DisplayPictureScreen(imagePath: state.imagePath);
        }

        if (state is TakeApictureLoaded) {
          return const TakePictureScreen();
        }

        return const Scaffold(body: Center(child: CircularProgressIndicator()));
      },
    );
  }
}

class TakePictureScreen extends StatefulWidget {
  const TakePictureScreen({
    super.key,
  });

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  late CameraController _controller;
  late Future<void> _initializeControllerFuture;

  @override
  void initState() {
    super.initState();

    _controller = CameraController(
      context.read<CameraRepository>().camera,
      ResolutionPreset.medium,
    );

    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(AppLocalizations.of(context)!.take_a_picture,
            style: Theme.of(context).textTheme.headlineSmall),
        automaticallyImplyLeading: false,
        actions: [
          IconButton(
              onPressed: () => Navigator.pop(context),
              icon: const Icon(CupertinoIcons.xmark_circle_fill))
        ],
      ),
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return CameraPreview(_controller);
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        onPressed: () async {
          try {
            await _initializeControllerFuture;

            final image = await _controller.takePicture();

            if (!mounted) return;

            context.read<OcrCubit>().onDisplayPictureLoaded(image.path);

            /*  await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DisplayPictureScreen(
                          imagePath: image.path,
                        )));

                        */
          } catch (e) {
            print(e);
          }
        },
        child: const Icon(
          Icons.camera_alt,
          color: Colors.white,
        ),
      ),
    );
  }
}

class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;

  const DisplayPictureScreen({super.key, required this.imagePath});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(AppLocalizations.of(context)!.confirm_text,
                style: Theme.of(context).textTheme.headlineSmall),
            automaticallyImplyLeading: false,
            actions: [
              IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: const Icon(CupertinoIcons.xmark_circle_fill))
            ]),
        body: Image.file(File(imagePath)),
        floatingActionButton: ElevatedButton(
          onPressed: () async {
            await processImage(File(imagePath)).then((value) {
              Navigator.pop(context, '$value');
            });
            return;
          },
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(AppLocalizations.of(context)!.confirm),
              const SizedBox(width: 10),
              const Icon(Icons.check),
            ],
          ),
        ));
  }

  Future processImage(File? file) async {
    final textRecognizer = TextRecognizer(script: TextRecognitionScript.latin);

    try {
      if (file != null) {
        InputImage? inputImage = InputImage.fromFile(file);

        final RecognizedText recognizedText =
            await textRecognizer.processImage(inputImage);

        String text = recognizedText.text;
        String resultText = '';

        for (TextBlock block in recognizedText.blocks) {
          for (TextLine line in block.lines) {
            for (TextElement element in line.elements) {
              resultText += element.text + ' ';
            }
          }
        }

        return resultText;
      }
    } catch (e) {
      debugPrint(e.toString());
    }
  }
}
