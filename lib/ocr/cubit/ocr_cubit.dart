import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'ocr_state.dart';

class OcrCubit extends Cubit<OcrState> {
  OcrCubit() : super(OcrInitial());

  void onTakeApictureLoaded() {
    emit(TakeApictureLoaded());
  }

  void onDisplayPictureLoaded(String imagePath) {
    emit(ValidateOcrLoaded(imagePath));
  }
}
