part of 'ocr_cubit.dart';

class OcrState extends Equatable {
  const OcrState();

  @override
  List<Object> get props => [];
}

class OcrInitial extends OcrState {}

class OcrLoading extends OcrState {}

class TakeApictureLoaded extends OcrState {}

class ValidateOcrLoaded extends OcrState {
  final String imagePath;

  const ValidateOcrLoaded(this.imagePath);

  @override
  List<Object> get props => [imagePath];
}
