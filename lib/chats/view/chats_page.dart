import 'package:ai_assistant/chats/widgets/chat_title.dart';
import 'package:ai_assistant/chats/widgets/chats_appbar.dart';
import 'package:ai_assistant/chats/widgets/chats_list.dart';
import 'package:flutter/material.dart';

class ChatsPage extends StatefulWidget {
  const ChatsPage({super.key});

  @override
  State<ChatsPage> createState() => _ChatsPageState();
}

class _ChatsPageState extends State<ChatsPage> {
  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        controller: scrollController,
        slivers: <Widget>[
          const ChatsAppbar(),
          const ChatTitle(),
          ChatsList(
            scrollController: scrollController,
          )
        ],
      ),
    );
  }
}
