part of 'chats_bloc.dart';

enum ChatsStatus { initial, loading, success, fail, empty }

class ChatsState extends Equatable {
  const ChatsState(
      {this.status = ChatsStatus.initial,
      this.convMetadataList = const [],
      this.hasReachedMax = false});

  final ChatsStatus status;
  final List<ConvMetadataFirebase> convMetadataList;
  final bool hasReachedMax;

  ChatsState copyWith(
      {ChatsStatus? status,
      List<ConvMetadataFirebase>? convMetadataList,
      bool? hasReachedMax}) {
    return ChatsState(
        status: status ?? this.status,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        convMetadataList: convMetadataList ?? this.convMetadataList);
  }

  @override
  List<Object> get props => [status, convMetadataList, hasReachedMax];
}
