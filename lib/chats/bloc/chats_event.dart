part of 'chats_bloc.dart';

abstract class ChatsEvent extends Equatable {
  const ChatsEvent();

  @override
  List<Object> get props => [];
}

class FetchedChatsData extends ChatsEvent {}

class RemovedChatsItem extends ChatsEvent {
  final int index;
  final String convID;
  const RemovedChatsItem({required this.index, required this.convID});
}
