import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import '../../model/conv_metadata_firebase.dart';
import '../../repository/auth_repository.dart';
import '../../repository/chat_repository.dart';

part 'chats_event.dart';
part 'chats_state.dart';

class ChatsBloc extends Bloc<ChatsEvent, ChatsState> {
  final ChatRepository _chatRepository;
  final AuthRepository _authRepository;

  ChatsBloc(
      {required ChatRepository chatRepository,
      required AuthRepository authRepository})
      : _chatRepository = chatRepository,
        _authRepository = authRepository,
        super(const ChatsState()) {
    on<FetchedChatsData>(_onFetchedChatsData);
    on<RemovedChatsItem>(_onRemovedChatsItem);
  }

  Future _onFetchedChatsData(
    FetchedChatsData event,
    Emitter<ChatsState> emit,
  ) async {
    if (state.hasReachedMax) return;

    try {
      final user = await _authRepository.fetchUserUID();

      switch (state.status) {
        case ChatsStatus.initial:
          if (user != null) {
            await _chatRepository.initChatsStream(
              user,
              null,
            );
          }

          await emit.forEach<List<ConvMetadataFirebase>>(
              _chatRepository.fetchConvMetadataList(),
              onData: (convMetadataList) {
            debugPrint('new data');

            convMetadataList.sort((a, b) => b.dtCreated.compareTo(a.dtCreated));

            return state.copyWith(
                status: convMetadataList.isEmpty
                    ? ChatsStatus.empty
                    : ChatsStatus.success,
                convMetadataList: convMetadataList,
                hasReachedMax: true);
          }, onError: (Object object, StackTrace stackTrace) {
            debugPrint(stackTrace.toString());
            return state.copyWith(
              status: ChatsStatus.empty,
              hasReachedMax: true,
            );
          });

          break;
        default:
          if (user != null) {
            final List<ConvMetadataFirebase> convMetadataList =
                await _chatRepository.initChatsStream(
              user,
              state.convMetadataList.last.convID,
            );

            emit(state.copyWith(
                hasReachedMax: convMetadataList.isEmpty ? true : false,
                status: state.convMetadataList.isEmpty
                    ? ChatsStatus.empty
                    : ChatsStatus.success));
          }
      }
    } catch (e) {
      debugPrint(e.toString());
      emit(state.copyWith(
        status: ChatsStatus.fail,
      ));
    }
  }

  Future _onRemovedChatsItem(
    RemovedChatsItem event,
    Emitter<ChatsState> emit,
  ) async {
    emit(state.copyWith(status: ChatsStatus.loading));
    String convID = event.convID;

    await _chatRepository.deleteChatMessage(
      convID,
    );
//TODO: CHANGE FALSE
    emit(state.copyWith(
        convMetadataList: state.convMetadataList, status: ChatsStatus.success));
  }
}
