import 'package:ai_assistant/chat/widgets/chat_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_swipe_action_cell/core/cell.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../chat/bloc/chat_bloc.dart';
import '../../chat/views/chat_page.dart';
import '../../model/conv_metadata_firebase.dart';
import '../../repository/chat_repository.dart';
import '../../utils/utils.dart';
import '../bloc/chats_bloc.dart';

class ChatsList extends StatefulWidget {
  final ScrollController scrollController;
  const ChatsList({super.key, required this.scrollController});

  @override
  State<ChatsList> createState() => _ChatsListState();
}

class _ChatsListState extends State<ChatsList> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatsBloc, ChatsState>(
      builder: (context, state) {
        switch (state.status) {
          case ChatsStatus.fail:
            return const SliverFillRemaining(
              child: Center(child: CircularProgressIndicator()),
            );

          case ChatsStatus.loading:
            return const SliverFillRemaining(
                child: Center(child: CircularProgressIndicator()));

          case ChatsStatus.empty:
            return SliverFillRemaining(
              child: Center(
                  child: Text(
                AppLocalizations.of(context)!.no_item_for_now,
                style: Theme.of(context).textTheme.titleLarge,
              )),
            );
          case ChatsStatus.success:
          default:
            return SliverPadding(
              padding: const EdgeInsets.only(top: 10),
              sliver: SliverList(
                delegate: SliverChildBuilderDelegate((context, index) {
                  return index >= state.convMetadataList.length
                      ? const _BottomLoader()
                      : SwipeActionCell(
                          key: ObjectKey(index),
                          trailingActions: <SwipeAction>[
                            SwipeAction(
                                title: AppLocalizations.of(context)!.delete,
                                onTap: (CompletionHandler handler) async {
                                  context.read<ChatsBloc>().add(
                                      RemovedChatsItem(
                                          index: index,
                                          convID: state
                                              .convMetadataList[index].convID));
                                },
                                color: CupertinoColors.destructiveRed),
                          ],
                          child:
                              _ChatsItem(item: state.convMetadataList[index]));
                },
                    childCount: state.hasReachedMax
                        ? state.convMetadataList.length
                        : state.convMetadataList.length + 1),
              ),
            );
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();
    widget.scrollController.addListener(_onScroll);
  }

  @override
  void dispose() {
    if (widget.scrollController.hasClients) {
      widget.scrollController
        ..removeListener(_onScroll)
        ..dispose();
    }

    super.dispose();
  }

  void _onScroll() {
    if (_isBottom) context.read<ChatsBloc>().add(FetchedChatsData());
  }

  bool get _isBottom {
    if (!widget.scrollController.hasClients) return false;
    final maxScroll = widget.scrollController.position.maxScrollExtent;
    final currentScroll = widget.scrollController.offset;

    return currentScroll >= (maxScroll * 0.9);
  }
}

class _ChatsItem extends StatelessWidget {
  const _ChatsItem({required this.item});

  final ConvMetadataFirebase item;

  @override
  Widget build(BuildContext context) {
    return CupertinoListTile(
      trailing: const CupertinoListTileChevron(),
      title: Text(
        item.answer.capitalize(),
        style: Theme.of(context).textTheme.bodyLarge,
      ),
      leading: Icon(
        CupertinoIcons.circle_fill,
        color: item.role == 'user'
            ? Theme.of(context).primaryColor
            : Theme.of(context).colorScheme.secondary,
      ),
      subtitle: Text(
        formatDate(DateTime.fromMillisecondsSinceEpoch(
          item.dtCreated,
        )),
        style: Theme.of(context).textTheme.bodySmall,
      ),
      onTap: () => Utils.navigatePage(
        context,
        BlocProvider(
          create: (context) => ChatBloc(
              chatRepository: context.read<ChatRepository>(),
              convID: item.convID)
            ..add(const UserDataLoaded()),
          child: const ChatPage(
            autofocus: false,
          ),
        ),
      ),
    );
  }

  String formatDate(DateTime date) {
    final formatter = DateFormat('HH:mm • MMMM dd, yyyy').format(date);

    return formatter;
  }
}

String capitalizeFirstLetter(String text) {
  String str =
      text.substring(0, 1).capitalize().toUpperCase() + text.substring(1);
  return str;
}

class _BottomLoader extends StatelessWidget {
  const _BottomLoader();

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.all(10.0),
      child: Center(
        child: SizedBox(
          height: 24,
          width: 24,
          child: CircularProgressIndicator(strokeWidth: 1.5),
        ),
      ),
    );
  }
}
