import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ChatTitle extends StatelessWidget {
  const ChatTitle({super.key});

  @override
  Widget build(BuildContext context) {
    return SliverPadding(
      padding: const EdgeInsets.only(left: 20),
      sliver: SliverToBoxAdapter(
        child: Text(
          AppLocalizations.of(context)!.all,
          style: Theme.of(context).textTheme.headlineSmall,
        ),
      ),
    );
  }
}
