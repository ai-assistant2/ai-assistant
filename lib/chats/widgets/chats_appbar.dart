import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../chat/bloc/chat_bloc.dart';
import '../../chat/views/chat_page.dart';
import '../../repository/chat_repository.dart';
import '../../utils/utils.dart';

class ChatsAppbar extends StatelessWidget {
  const ChatsAppbar({super.key});

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      title: Text(
        AppLocalizations.of(context)!.chats,
        style: Theme.of(context)
            .textTheme
            .headlineMedium
            ?.copyWith(fontWeight: FontWeight.bold),
        maxLines: 1,
      ),
      centerTitle: false,
      actions: [
        IconButton(
            onPressed: () => Utils.navigatePage(
                context,
                BlocProvider(
                  create: (context) => ChatBloc(
                      chatRepository: context.read<ChatRepository>(),
                      convID: null)
                    ..add(const UserDataLoaded()),
                  child: const ChatPage(
                    autofocus: true,
                  ),
                )),
            icon: Icon(
              CupertinoIcons.add,
              color: Theme.of(context).primaryColor,
            ))
      ],
    );
  }
}
