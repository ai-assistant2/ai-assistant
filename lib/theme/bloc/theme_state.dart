part of 'theme_bloc.dart';

enum ThemeStatus { initial, loading }

class ThemeState extends Equatable {
  const ThemeState(
      {this.themeData,
      this.themeStatus = ThemeStatus.initial,
      this.locale = const Locale('en', 'EN')});

  final ThemeData? themeData;
  final Locale locale;
  final ThemeStatus themeStatus;

  ThemeState copyWith(
      {ThemeData? themeData, ThemeStatus? themeStatus, Locale? locale}) {
    return ThemeState(
        locale: locale ?? this.locale,
        themeData: themeData ?? this.themeData,
        themeStatus: themeStatus ?? this.themeStatus);
  }

  @override
  List<Object?> get props => [themeData, themeStatus, locale];
}
