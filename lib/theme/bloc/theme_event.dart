part of 'theme_bloc.dart';

abstract class ThemeEvent extends Equatable {
  const ThemeEvent();

  @override
  List<Object> get props => [];
}

class ThemeLoading extends ThemeEvent {}

class FetchTheme extends ThemeEvent {}

class ThemeChanged extends ThemeEvent {
  final AppTheme theme;

  const ThemeChanged({required this.theme});
}

class LanguageChanged extends ThemeEvent {
  final Locale locale;
  const LanguageChanged(this.locale);
}

class LanguageStartChanged extends ThemeEvent {}
