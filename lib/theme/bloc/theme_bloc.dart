import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../theme_data/app_theme.dart';

part 'theme_event.dart';
part 'theme_state.dart';

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {
  late SharedPreferences _prefs;
  late bool isDarkTheme;

  ThemeBloc() : super(const ThemeState()) {
    on<FetchTheme>(_onFetchTheme);
    on<ThemeChanged>(_onChangedTheme);
    on<LanguageChanged>(_onChangeLang);
    on<LanguageStartChanged>(_onChangeStartLang);
  }

  void _onFetchTheme(FetchTheme event, Emitter emit) async {
    _prefs = await SharedPreferences.getInstance();

    bool checkIfThemeExists = _prefs.containsKey('isDarkTheme');

    if (checkIfThemeExists) {
      isDarkTheme = _prefs.getBool('isDarkTheme') ?? true;
      // print(' on theme$isDarkTheme');
      isDarkTheme
          ? emit(ThemeState(themeData: appThemeData[AppTheme.blueDark]))
          : emit(ThemeState(themeData: appThemeData[AppTheme.blueLight]));
    } else {
      isDarkTheme = true;
      emit(ThemeState(themeData: appThemeData[AppTheme.blueDark]));
    }
  }

  void _onChangedTheme(ThemeChanged event, Emitter emit) async {
    switch (event.theme) {
      case AppTheme.blueLight:
        emit(ThemeState(themeData: appThemeData[event.theme]));
        // _prefs = await SharedPreferences.getInstance();
        _prefs.setBool('isDarkTheme', false);

        break;

      case AppTheme.blueDark:
        emit(ThemeState(themeData: appThemeData[event.theme]));
        // _prefs = await SharedPreferences.getInstance();
        _prefs.setBool('isDarkTheme', true);

        break;

      default:
    }
  }

  void _onChangeStartLang(LanguageStartChanged event, Emitter emit) async {
    _prefs = await SharedPreferences.getInstance();
    String? langCode = _prefs.getString('lang');

    if (langCode != null) {
      emit(state.copyWith(locale: Locale(langCode, '')));
    } else {
      final String defaultSystemLocale = Platform.localeName;
      _prefs.setString(defaultSystemLocale, '');
      emit(state.copyWith(
        locale: Locale(defaultSystemLocale, ''),
      ));
    }
  }

  void _onChangeLang(LanguageChanged event, Emitter emit) async {
    emit(state.copyWith(locale: event.locale));
    _prefs = await SharedPreferences.getInstance();
    await _prefs.setString('lang', event.locale.languageCode);
  }
}
