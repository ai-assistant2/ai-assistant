import 'package:ai_assistant/network_connection/page/network_connection_page.dart';
import 'package:ai_assistant/repository/auth_repository.dart';
import 'package:ai_assistant/repository/chat_repository.dart';
import 'package:ai_assistant/repository/purchases_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import '../../ads/bloc/ads_bloc.dart';
import '../../auth/bloc/auth_bloc.dart';
import '../../network_connection/bloc/network_connection_bloc.dart';
import '../../purchases/bloc/purchases_bloc.dart';
import '../../utils/utils.dart';
import '../bloc/theme_bloc.dart';

class ThemePage extends StatelessWidget {
  const ThemePage(
      {super.key,
      required this.purchasesRepository,
      required this.chatRepository,
      required this.authRepository});
  final ChatRepository chatRepository;
  final AuthRepository authRepository;
  final PurchasesRepository purchasesRepository;
  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider.value(
          value: authRepository,
        ),
        RepositoryProvider.value(
          value: chatRepository,
        ),
        RepositoryProvider.value(value: purchasesRepository),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) =>
                AuthBloc(authRepository: context.read<AuthRepository>())
                  ..add(AppLoaded()),
          ),
          BlocProvider(
            create: (context) => AdsBloc()..add(AdsLoaded()),
          ),
          BlocProvider(
            create: (context) => PurchasesBloc(
                purchasesRepository: context.read<PurchasesRepository>())
              ..add(PurchasesInitialized()),
          )
        ],
        child: BlocBuilder<ThemeBloc, ThemeState>(
          builder: (context, state) {
            return MaterialApp(
                debugShowCheckedModeBanner: false,
                localizationsDelegates: const [
                  AppLocalizations.delegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                ],
                supportedLocales: const [
                  Locale('en', 'EN'),
                  Locale('ar', 'SA'),
                  Locale('es', 'ES'),
                  Locale('fr', 'FR'),
                  Locale('de', 'DE'),
                  Locale('zh', 'CN'),
                  Locale('ko', 'KP'),
                  Locale('it', 'IT'),
                  Locale('pt', 'PT'),
                  Locale('ja', 'JP'),
                ],
                locale: state.locale,
                title: 'Iris AI',
                theme: state.themeData,
                home: state.themeStatus == ThemeStatus.loading
                    ? Utils.loadingWidget()
                    : BlocProvider(
                        create: (context) => NetworkConnectionBloc()
                          ..add(NetworkConnectionChecked()),
                        child: const NetworkConnectionPage(),
                      ));
          },
        ),
      ),
    );
  }
}
