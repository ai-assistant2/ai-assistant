import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum AppTheme {
  blueLight,
  blueDark,
}

final appThemeData = {
  AppTheme.blueLight: ThemeData(
      brightness: Brightness.light,
      // backgroundColor: const Color.fromRGBO(239, 241, 241, 1),
      fontFamily: "SFPro-Regular",
      colorScheme: const ColorScheme.light(
          inversePrimary: Color.fromARGB(255, 181, 181, 181),
          secondary: Color.fromRGBO(0, 135, 126, 1),
          tertiary: Color.fromRGBO(21, 30, 47, 1)),
      primaryColor: const Color.fromRGBO(95, 75, 139, 1),
      splashColor: const Color.fromARGB(255, 234, 233, 233),
      cardTheme: const CardTheme(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(15))),
      ),
      cardColor: CupertinoColors.systemGrey5,
      scaffoldBackgroundColor: const Color.fromRGBO(242, 241, 246, 1),
      tabBarTheme: const TabBarTheme(labelColor: CupertinoColors.systemGrey),
      floatingActionButtonTheme: const FloatingActionButtonThemeData(
          foregroundColor: CupertinoColors.white,
          backgroundColor: CupertinoColors.activeBlue),
      iconTheme: const IconThemeData(color: Colors.black),
      buttonTheme: const ButtonThemeData(buttonColor: Colors.black),
      textTheme: const TextTheme(
          headlineMedium: TextStyle(
            color: Colors.black,
          ),
          headlineLarge: TextStyle(
            color: Colors.black,
          )),
      appBarTheme: const AppBarTheme(
          iconTheme: IconThemeData(color: Colors.black),
          backgroundColor: CupertinoColors.white,
          titleTextStyle: TextStyle(color: CupertinoColors.black)),
      inputDecorationTheme: const InputDecorationTheme(
        labelStyle: TextStyle(color: CupertinoColors.activeBlue),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
              style: BorderStyle.solid, color: CupertinoColors.activeBlue),
        ),
      ),
      progressIndicatorTheme: const ProgressIndicatorThemeData(
        color: Color.fromARGB(255, 61, 87, 58),
      ),
      textButtonTheme: TextButtonThemeData(
          style: ButtonStyle(
              textStyle: MaterialStateTextStyle.resolveWith(
                  (states) => const TextStyle(color: Colors.white)))),
      bottomSheetTheme: const BottomSheetThemeData(
          backgroundColor: Color.fromRGBO(255, 254, 255, 1)),
      cupertinoOverrideTheme: const CupertinoThemeData(
          scaffoldBackgroundColor: CupertinoColors.white,
          primaryColor: CupertinoColors.activeBlue)),
  AppTheme.blueDark: ThemeData(
      brightness: Brightness.dark,
      // backgroundColor: const Color.fromRGBO(24, 24, 26, 1),
      fontFamily: "SFPro-Regular",
      dividerColor: const Color.fromRGBO(43, 43, 43, 1),
      primaryColor: const Color.fromRGBO(95, 75, 139, 1),
      splashColor: Colors.grey.shade900,
      colorScheme: const ColorScheme.dark(
        inversePrimary: Color.fromRGBO(7, 7, 7, 1),
        secondary: Color.fromRGBO(0, 135, 126, 1),
        tertiary: Color.fromRGBO(21, 30, 47, 1),
      ),
      textTheme: const TextTheme(
        headlineMedium: TextStyle(
          color: Colors.white,
        ),
        headlineLarge: TextStyle(
          color: Colors.white,
        ),
      ),
      textButtonTheme: TextButtonThemeData(
          style: ButtonStyle(
        textStyle: MaterialStateProperty.all<TextStyle>(
            const TextStyle(color: Colors.white)),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        )),
        backgroundColor: MaterialStateProperty.all(const Color.fromRGBO(
          31,
          31,
          31,
          1,
        )),
      )),
      cardTheme: const CardTheme(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15))),
          color: Color.fromARGB(
            255,
            28,
            28,
            30,
          )),
      progressIndicatorTheme: const ProgressIndicatorThemeData(
        color: Color.fromRGBO(95, 75, 139, 1),
      ),
      scaffoldBackgroundColor: const Color.fromARGB(255, 0, 0, 0),
      tabBarTheme: const TabBarTheme(labelColor: CupertinoColors.white),
      inputDecorationTheme:
          const InputDecorationTheme(fillColor: Color.fromRGBO(43, 43, 43, 1)),
      buttonTheme:
          const ButtonThemeData(buttonColor: Color.fromRGBO(121, 130, 150, 1)),
      floatingActionButtonTheme: const FloatingActionButtonThemeData(
          backgroundColor: Color.fromRGBO(121, 130, 150, 1)),
      appBarTheme: AppBarTheme(
          backgroundColor: Colors.grey.shade900,
          titleTextStyle: const TextStyle(color: CupertinoColors.white)),
      bottomNavigationBarTheme: const BottomNavigationBarThemeData(
          backgroundColor: Color.fromRGBO(23, 23, 23, 1)),
      cupertinoOverrideTheme:
          const CupertinoThemeData(primaryColor: CupertinoColors.activeBlue)),
};
