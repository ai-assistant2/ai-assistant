import 'package:app_settings/app_settings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../auth/views/auth_page.dart';
import '../../repository/purchases_repository.dart';
import '../../utils/utils.dart';
import '../bloc/network_connection_bloc.dart';

class NetworkConnectionPage extends StatefulWidget {
  const NetworkConnectionPage({super.key});

  @override
  State<NetworkConnectionPage> createState() => _NetworkConnectionPageState();
}

class _NetworkConnectionPageState extends State<NetworkConnectionPage>
    with WidgetsBindingObserver {
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.resumed) {
      context.read<NetworkConnectionBloc>().add(NetworkConnectionChecked());
      bool isDialogOpen = _isThereCurrentDialogShowing(context);
      if (!isDialogOpen) {
        bool isPremium =
            await context.read<PurchasesRepository>().getSubscriptionStatus();
        isPremium == false ? Utils.showPurchasePage(context) : null;
      }
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NetworkConnectionBloc, NetworkConnectionState>(
      builder: (context, state) {
        switch (state.status) {
          case NetworkConnectionStatus.isConnected:
            return const AuthPage();

          case NetworkConnectionStatus.isNotConnected:
            return Scaffold(
              body: Center(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 35, vertical: 10),
                    child: Text(
                      AppLocalizations.of(context)!.oups_no_internet_connection,
                      textAlign: TextAlign.justify,
                      style: Theme.of(context).textTheme.bodyMedium,
                    ),
                  ),
                  CupertinoButton(
                    color: Theme.of(context).primaryColor,
                    onPressed: () {
                      AppSettings.openAppSettings();
                    },
                    child: Text(
                        AppLocalizations.of(context)!.open_wireless_settings),
                  )
                ],
              )),
            );

          case NetworkConnectionStatus.initial:
          default:
            return const Scaffold(
                body: Center(child: CircularProgressIndicator()));
        }
      },
    );
  }

  _isThereCurrentDialogShowing(BuildContext context) =>
      ModalRoute.of(context)?.isCurrent != true;
}
