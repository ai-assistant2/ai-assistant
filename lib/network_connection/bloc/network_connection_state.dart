part of 'network_connection_bloc.dart';

enum NetworkConnectionStatus { initial, isConnected, isNotConnected }

class NetworkConnectionState extends Equatable {
  const NetworkConnectionState({this.status = NetworkConnectionStatus.initial});

  final NetworkConnectionStatus status;

  NetworkConnectionState copyWith({NetworkConnectionStatus? status}) {
    return NetworkConnectionState(status: status ?? this.status);
  }

  @override
  List<Object> get props => [status];
}
