import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:equatable/equatable.dart';

part 'network_connection_event.dart';
part 'network_connection_state.dart';

class NetworkConnectionBloc
    extends Bloc<NetworkConnectionEvent, NetworkConnectionState> {
  NetworkConnectionBloc() : super(const NetworkConnectionState()) {
    on<NetworkConnectionChecked>(_onNetworkConnectionChecked);
  }

  void _onNetworkConnectionChecked(NetworkConnectionChecked event,
      Emitter<NetworkConnectionState> emit) async {
    if (state.status != NetworkConnectionStatus.isConnected) {
      emit(state.copyWith(status: NetworkConnectionStatus.initial));
      try {
        var connectivityResult = await Connectivity().checkConnectivity();

        await Connectivity().checkConnectivity().then((value) async {
          if (connectivityResult == ConnectivityResult.none) {
            try {
              var connectivityResult = await Connectivity().checkConnectivity();

              Future.delayed(
                  const Duration(seconds: 5),
                  await Connectivity().checkConnectivity().then((value) {
                    connectivityResult == ConnectivityResult.none
                        ? emit(state.copyWith(
                            status: NetworkConnectionStatus.isNotConnected))
                        : emit(state.copyWith(
                            status: NetworkConnectionStatus.isConnected));
                    return;
                  }));
            } catch (e) {
              emit(state.copyWith(
                  status: NetworkConnectionStatus.isNotConnected));
            }

            emit(
                state.copyWith(status: NetworkConnectionStatus.isNotConnected));
          } else {
            emit(state.copyWith(status: NetworkConnectionStatus.isConnected));
          }
        });
      } catch (e) {
        emit(state.copyWith(status: NetworkConnectionStatus.isNotConnected));
      }
    }
  }
}
