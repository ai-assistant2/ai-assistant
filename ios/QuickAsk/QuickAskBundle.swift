//
//  QuickAskBundle.swift
//  QuickAsk
//
//  Created by MACAIR on 22/09/2023.
//

import WidgetKit
import SwiftUI

@main
struct QuickAskBundle: WidgetBundle {
    var body: some Widget {
        QuickAsk()
        QuickAskLiveActivity()
    }
}
