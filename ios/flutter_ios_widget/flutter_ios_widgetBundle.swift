//
//  flutter_ios_widgetBundle.swift
//  flutter_ios_widget
//
//  Created by MACAIR on 23/09/2023.
//

import WidgetKit
import SwiftUI

@main
struct flutter_ios_widgetBundle: WidgetBundle {
    var body: some Widget {
        flutter_ios_widget()
    }
}
